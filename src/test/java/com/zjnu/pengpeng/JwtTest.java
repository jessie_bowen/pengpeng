package com.zjnu.pengpeng;

import com.zjnu.pengpeng.controller.entity.UserVO;
import com.zjnu.pengpeng.util.JwtUtils;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import org.junit.jupiter.api.Test;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author ZhaoBW
 * @version 1.0
 * @date 2020/11/27 17:51
 */
public class JwtTest {


//    @Test
    void test(){
        //生成token
//        UserVO u = new UserVO();
//        u.setId(1);
//        u.setNick_name("赵博文");
//        u.setSex("男");
//        u.setIcon("www.123.com");
//        String token = JwtUtils.generateWebToken(u);
//        System.out.println(token);

        Claims claims = JwtUtils.checkJWT("eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJwZW5ncGVuZyIsImlkIjoxLCJuaWNrTmFtZSI6Iui1teWNmuaWhyIsInNleCI6IueUtyIsImljb24iOiJ3d3cuMTIzLmNvbSIsImlhdCI6MTYwNjU2MTQ4NCwiZXhwIjoxNjE0NTEwMjg0fQ.dEdxfj8q5K1ALdRTg00U3WnuMP8jqho7Rzy5mAbGeLs");
        Date date1 = claims.getIssuedAt();
        Date date2 = claims.getExpiration();

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");

        System.out.println(sdf.format(date1));
        System.out.println(sdf.format(date2));

//
//        //校验token
//        Claims claims = JwtUtils.checkJWT(token);
//        System.out.println(claims);
//        System.out.println("id: "+claims.get("id"));
//        System.out.println("nickName: "+claims.get("nickName"));
//        System.out.println("sex: "+claims.get("sex"));
//        System.out.println("icon: "+claims.get("icon"));
//        System.out.println(JwtUtils.checkJWT("eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJwZW5ncGVuZyIsImlkIjoxLCJuaWNrTmFtZSI6Iui1teWNmuaWhyIsInNleCI6IueUtyIsImljb24iOiJ3d3cuMTIzLmNvbSIsImlhdCI6MTYwNjQ5NTQ4MCwiZXhwIjoxNjA2NDk1NjAwfQ.6rUOaAFmFKiAX1RG27LIg0j--pmyW8wapAHO6sJS27c"));
    }

}
