package com.zjnu.pengpeng;

import com.zjnu.pengpeng.dao.entity.UserDO;
import com.zjnu.pengpeng.dao.mapper.UserMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @author ZhaoBW
 * @version 1.0
 * @date 2020/11/28 0:12
 */
//@SpringBootTest
public class UserTest {

//    @Autowired
    UserMapper userMapper;

//    @Test
    void test(){
        UserDO user = new UserDO();
        user.setNickName("nickName");
        user.setSex("男");
        user.setIcon("icon");
        user.setOpenid("openid");
        userMapper.insertUser(user);
        System.out.println(user);
    }

}
