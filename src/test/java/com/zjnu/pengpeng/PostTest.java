package com.zjnu.pengpeng;

import com.zjnu.pengpeng.dao.mapper.PostMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @author ZhaoBW
 * @version 1.0
 * @date 2020/12/17 19:10
 */
@SpringBootTest
public class PostTest {
    @Autowired
    PostMapper mapper;

    @Test
    void test(){
        mapper.updateLabel(1);
    }
}
