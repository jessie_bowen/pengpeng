package com.zjnu.pengpeng;

import com.alibaba.fastjson.JSONObject;
import com.zjnu.pengpeng.util.WechatUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.junit.jupiter.api.Test;

import java.io.IOException;

/**
 * @author ZhaoBW
 * @version 1.0
 * @date 2020/12/3 13:34
 */
public class HttpTest {

//    @Test
    void testContent(){
        String access_token = WechatUtils.getAccessToken();
        String url = "https://api.weixin.qq.com/wxa/msg_sec_check?access_token=" + access_token;
        //创建客户端
        HttpClient httpclient = HttpClients.createDefault();
        //创建一个post请求
        HttpPost request = new HttpPost(url);
        //设置响应头
        request.setHeader("Content-Type", "application/json;charset=UTF-8");
        //通过fastJson设置json数据
        JSONObject postData = new JSONObject();
        //设置要检测的内容
        postData.put("content", "相声里面高清监控的偷拍隐蔽是很难的，单纯厂家就已经在线下注够难竞猜投注了");
        String jsonString = postData.toString();
        request.setEntity(new StringEntity(jsonString,"utf-8"));
        try {
            HttpResponse response = httpclient.execute(request);
            // 从响应模型中获取响应实体
            HttpEntity entity = response.getEntity();
            //得到响应结果
            String result = EntityUtils.toString(entity,"utf-8");
            //打印检测结果
            System.out.println("检测结果:"+result);
            //将响应结果变成json
            JSONObject resultJsonObject = JSONObject.parseObject(result);

            String errcode =resultJsonObject.getString("errcode");
            if(errcode.equals("87014")){//当content内含有敏感信息，则返回87014
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

//    @Test
    void testImage(){
        String access_token = "39_16veoYrkLG-_JHlG8IX0aZebzA_y_SW1o4P2vLEA4Nf2hksuC2ta6fv54AxTFWsrQ0hXhPOCI4dptxFrpVUl-gNNHNGJNNg1MjZGPFosdRDUQh88i1SYEWxuEeRsHGhpyicBGLVo--GbxEa5PWHcAJABIT";
        String url = "https://api.weixin.qq.com/wxa/msg_sec_check?access_token=" + access_token;
        //创建客户端
        HttpClient httpclient = HttpClients.createDefault();
        //创建一个post请求
        HttpPost request = new HttpPost(url);
        //设置响应头
        request.setHeader("Content-Type", "application/json;charset=UTF-8");
        //通过fastJson设置json数据
        JSONObject postData = new JSONObject();
        //设置要检测的内容
        postData.put("content", "今天又是好日子");
        String jsonString = postData.toString();
        request.setEntity(new StringEntity(jsonString,"utf-8"));
        try {
            HttpResponse response = httpclient.execute(request);
            // 从响应模型中获取响应实体
            HttpEntity entity = response.getEntity();
            //得到响应结果
            String result = EntityUtils.toString(entity,"utf-8");
            //打印检测结果
            System.out.println("检测结果:"+result);
            //将响应结果变成json
            JSONObject resultJsonObject = JSONObject.parseObject(result);

            String errcode =resultJsonObject.getString("errcode");
            if(errcode.equals("87014")){//当content内含有敏感信息，则返回87014
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
