package com.zjnu.pengpeng;

import com.zjnu.pengpeng.util.CommonUtils;
import com.zjnu.pengpeng.util.HttpUtils;
import com.zjnu.pengpeng.util.WechatUtils;
import org.junit.jupiter.api.Test;
import org.springframework.util.StringUtils;

import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 * @author ZhaoBW
 * @version 1.0
 * @date 2020/11/19 21:41
 */
public class WechatTest {

//    @Test
    void test() throws NoSuchAlgorithmException {
        String timeStamp = String.valueOf(System.currentTimeMillis());

        SortedMap<String,String> params = new TreeMap<>();
        params.put("appId","wx1f58fcb342a8478b");
        params.put("timeStamp",timeStamp);
        params.put("nonceStr","pyiDiRta5CwXuEay");
        params.put("package","prepay_id=wx19213457719545eff5b5c44f73b8af0000");
        params.put("signType","MD5");

        StringBuilder sb = new StringBuilder();
        for(Map.Entry<String,String> entry: params.entrySet()){
            String key = entry.getKey();
            String value = entry.getValue();
            if(!StringUtils.isEmpty(value)){
                sb.append(key + "=" + value + "&");
            }
        }
        System.out.println(timeStamp);
        String str = sb.toString();
        System.out.println(str);
        String str1 = str.substring(0,str.length()-1);
        System.out.println(str1);
        System.out.println(CommonUtils.MD5(str1));
    }

//    @Test
    void testGetOpenid(){
        String url = "https://api.weixin.qq.com/sns/jscode2session";
        Map<String,String> param = new HashMap<>();
        param.put("appid","wx1f58fcb342a8478b");
        param.put("secret","09425a8d3006911cdf3b69217f9bff4d");
        param.put("js_code","051JWNFa19Kh3A0SwMHa1lbpv74JWNFr");
        param.put("grant_type","authorization_code");
        String finalUrl = HttpUtils.generateGetParam(url,param);
        Map<String, Object> stringObjectMap = HttpUtils.doGet(finalUrl);
        System.out.println(stringObjectMap);
    }


//    @Test
    void testAccessToken(){
        String accessToken = WechatUtils.getAccessToken();
        System.out.println(accessToken);

//        WechatUtils.checkContent("特3456书yuuo莞6543李zxcz蒜7782法fgnv级");
//        Map<String,String> param = new HashMap<>(2);
//        param.put("access_token","39_H55t5lp31DWQxOZ-HVMVO3oZkLktR16sucg4tvx9sUUw150V5XxNJnedYsQBrXJZgS_1iPsWSHAkV-aBaeBsuHmljG6GPnyHjA82YoxCyvNEVmmLzCn-SN4CQW4o5UjqVYsAuiwVcQMuUMOWJQCgAIASZL");
//        param.put("content","特3456书yuuo莞6543李zxcz蒜7782法fgnv级");
//        String paramStr = HttpUtils.generatePostParam(param);
//        System.out.println(paramStr);

    }

}
