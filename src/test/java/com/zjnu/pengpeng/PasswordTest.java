package com.zjnu.pengpeng;

import org.junit.jupiter.api.Test;
import org.springframework.security.crypto.bcrypt.BCrypt;

/**
 * @author ZhaoBW
 * @version 1.0
 * @date 2020/11/26 9:01
 */
public class PasswordTest {

//    @Test
    void test(){
        String hashPassword = BCrypt.hashpw("123456", BCrypt.gensalt());
        System.out.println(hashPassword);
        boolean checkPassword = BCrypt.checkpw("123456", hashPassword);
        System.out.println(checkPassword);
    }
}
