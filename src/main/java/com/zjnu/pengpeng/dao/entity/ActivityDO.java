package com.zjnu.pengpeng.dao.entity;

import lombok.Data;

/**
 * @author ZhaoBW
 * @version 1.0
 * @date 2020/12/16 8:56
 */
@Data
public class ActivityDO {

    private Integer id;
    private String name;
    private String image;
    private Integer isDeleted;

}
