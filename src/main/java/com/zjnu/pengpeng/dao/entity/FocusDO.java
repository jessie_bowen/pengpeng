package com.zjnu.pengpeng.dao.entity;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @author zbw
 */
@TableName("focus")
@Data
public class FocusDO {

    @TableId(type = IdType.AUTO)
    private Integer id;
    private Integer userId;
    private Integer focusUserId;
    private Integer isFocusEachOther;

}
