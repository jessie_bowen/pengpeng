package com.zjnu.pengpeng.dao.entity;

import com.zjnu.pengpeng.controller.entity.BriefUserVO;
import lombok.Data;

/**
 * @author ZhaoBW
 * @version 1.0
 * @date 2020/11/9 10:27
 */
@Data
public class BriefUserDO {
    private Integer userId;
    private String nickName;
    private String userIcon;
    private String userAddress;
    private String sex;

    public static BriefUserDO convertUser(BriefUserVO controllerObject){
        BriefUserDO daoObject = new BriefUserDO();
        daoObject.setUserId(controllerObject.getUser_id());
        daoObject.setNickName(controllerObject.getNick_name());
        daoObject.setUserIcon(controllerObject.getIcon());
        daoObject.setSex(controllerObject.getSex());
        return daoObject;
    }

}
