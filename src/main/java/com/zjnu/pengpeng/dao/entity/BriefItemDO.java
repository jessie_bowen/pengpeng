package com.zjnu.pengpeng.dao.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * @author ZhaoBW
 * @version 1.0
 * @date 2020/12/7 20:09
 */
@TableName("item")
@Data
public class BriefItemDO implements Serializable {

    @TableId(type = IdType.AUTO)
    private Integer id;
    private String name;
    private String coverImg;
    private Integer originalPrice;
    private Integer sellPrice;
}
