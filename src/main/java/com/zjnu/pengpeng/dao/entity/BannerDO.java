package com.zjnu.pengpeng.dao.entity;

import lombok.Data;

/**
 * @author ZhaoBW
 * @version 1.0
 * @date 2020/11/6 14:54
 */
@Data
public class BannerDO {
    private Integer businessId;
    private String url;

}
