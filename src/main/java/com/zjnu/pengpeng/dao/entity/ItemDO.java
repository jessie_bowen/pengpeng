package com.zjnu.pengpeng.dao.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * @author ZhaoBW
 * @version 1.0
 * @date 2020/12/7 20:58
 */
@TableName("item")
@Data
public class ItemDO implements Serializable {

    @TableId(type = IdType.AUTO)
    private Integer id;
    private Integer categoryId;
    private String name;
    private String coverImg;
    private Integer originalPrice;
    private Integer sellPrice;
    private Integer sellCount;
    private String image;
    private String originPlace;
    private Integer rank;
    private String summary;
    private String specification;
    private String weight;
    private String packages;
    private String expirationDate;
    private String storageMethod;
    private Integer isOffShell;
    private Integer stock;

}
