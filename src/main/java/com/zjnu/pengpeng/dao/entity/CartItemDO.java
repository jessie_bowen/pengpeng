package com.zjnu.pengpeng.dao.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.models.auth.In;
import lombok.Data;

/**
 * @author ZhaoBW
 * @version 1.0
 * @date 2020/12/8 9:15
 */
@TableName("shopping_cart")
@Data
public class CartItemDO {

    @TableId(type = IdType.AUTO)
    private Integer id;
    private Integer userId;
    private Integer itemId;
    private String itemName;
    private String itemImg;
    private Integer unitPrice;
    private Integer count;
    private Integer isDeleted;

    public static CartItemDO convertCartItem(int userId, ItemDO itemDO){

        CartItemDO cartItemDO = new CartItemDO();
        cartItemDO.setUserId(userId);
        cartItemDO.setItemId(itemDO.getId());
        cartItemDO.setCount(1);
        cartItemDO.setItemImg(itemDO.getCoverImg());
        cartItemDO.setItemName(itemDO.getName());
        cartItemDO.setUnitPrice(itemDO.getSellPrice());

        return cartItemDO;
    }

}
