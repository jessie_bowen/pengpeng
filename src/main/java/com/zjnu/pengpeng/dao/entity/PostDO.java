package com.zjnu.pengpeng.dao.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.zjnu.pengpeng.controller.entity.PostVO;
import com.zjnu.pengpeng.util.StringListConvertUtils;
import lombok.Data;

/**
 * @author ZhaoBW
 * @version 1.0
 * @date 2020/11/6 8:15
 */
@Data
@TableName("post")
public class PostDO {

    @TableId(type = IdType.AUTO)
    private Integer id;
    private Integer userId;
    private String icon;
    private String nickName;
    private Integer labelId;
    private String content;
    private Integer praiseCount;
    private Integer commentCount;
    private String time;
    private String images;

    public static PostDO convertDaoEntity(PostVO controllerEntity){
        PostDO result = new PostDO();
        result.setId(controllerEntity.getId());
        result.setUserId(controllerEntity.getUser_id());
        result.setLabelId(controllerEntity.getLabel_id());
        result.setContent(controllerEntity.getContent());
        result.setPraiseCount(controllerEntity.getPraise_count());
        result.setCommentCount(controllerEntity.getComment_count());
        result.setTime(controllerEntity.getTime());
        result.setImages(StringListConvertUtils.listToString(controllerEntity.getImgs()));
        return result;
    }

}
