package com.zjnu.pengpeng.dao.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * @author ZhaoBW
 * @version 1.0
 * @date 2020/12/7 18:54
 */
@TableName("category")
@Data
public class CategoryDO implements Serializable {

    @TableId(type = IdType.AUTO)
    private Integer id;
    private Integer shopId;
    private String name;
    private Integer isDeleted;

}
