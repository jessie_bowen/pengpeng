package com.zjnu.pengpeng.dao.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.zjnu.pengpeng.controller.entity.TaskVO;
import lombok.Data;

/**
 * @author ZhaoBW
 * @version 1.0
 * @date 2020/11/8 15:24
 */
@Data
@TableName("task")
public class TaskDO {

    @TableId(type = IdType.AUTO)
    private Integer id;

    private String address;
    private String content;
    private Integer state;
    private String phone;
    private Integer userId;
    private String nickName;
    private String userIcon;
    private String userSex;
    private Integer receiverId;
    private String receiverNickName;
    private String receiverIcon;
    private String receiverPhone;
    private String receiverAddress;
    private String receiverSex;
    private Double reward;
    private String time;
    private String category;

    public static TaskDO convertDO(TaskVO voEntity){
        TaskDO doEntity = new TaskDO();
        doEntity.setId(voEntity.getId());
        doEntity.setUserId(voEntity.getUser_id());
        doEntity.setAddress(voEntity.getAddress());
        doEntity.setContent(voEntity.getContent());
        doEntity.setState(voEntity.getState());
        doEntity.setPhone(voEntity.getPhone());
        doEntity.setNickName(voEntity.getNick_name());
        doEntity.setUserIcon(voEntity.getUser_icon());
        doEntity.setReceiverId(voEntity.getReceiver_id());
        doEntity.setReceiverNickName(voEntity.getReceiver_nick_name());
        doEntity.setReceiverIcon(voEntity.getReceiver_icon());
        doEntity.setReceiverPhone(voEntity.getReceiver_phone());
        doEntity.setReceiverAddress(voEntity.getReceiver_address());
        doEntity.setReward(voEntity.getReward());
        doEntity.setTime(voEntity.getTime());
        doEntity.setCategory(voEntity.getCategory());
        return doEntity;
    }


}
