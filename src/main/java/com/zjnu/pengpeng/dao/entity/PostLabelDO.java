package com.zjnu.pengpeng.dao.entity;

import io.swagger.models.auth.In;
import lombok.Data;

/**
 * @author ZhaoBW
 * @version 1.0
 * @date 2020/11/6 16:37
 */
@Data
public class PostLabelDO {

    private Integer id;
    private String name;
    private String icon;
    private Integer postNum;
    private String lastUpdateTime;
    private Integer isDeleted;

}
