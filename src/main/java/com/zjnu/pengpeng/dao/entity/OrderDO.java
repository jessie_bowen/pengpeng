package com.zjnu.pengpeng.dao.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.zjnu.pengpeng.controller.entity.OrderVO;
import com.zjnu.pengpeng.util.CommonUtils;
import lombok.Data;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * @author ZhaoBW
 * @version 1.0
 * @date 2020/11/18 19:16
 */
@Data
@TableName("order")
public class OrderDO {

    @TableId(type = IdType.AUTO)
    private Integer id;
    private String appid;
    /**
     * 64位订单唯一标识
     */
    private String tradeNo;
    private Integer state;
    /**
     * 微信后台通知支付结果时间
     */
    private String notifyTime;
    /**
     * 订单总额：以分为单位
     * （与微信平台统一字段类型）
     */
    private Integer totalFee;
    private Integer userId;
    private String nickName;
    private String icon;
    private Integer addressId;
    private String createTime;
    private Integer isDeleted;
    private String ip;
    private List<OrderDetailDO> orderDetailList;

    public static OrderDO convertDO(OrderVO orderVO,String appid){
        OrderDO orderDO = new OrderDO();
        orderDO.setId(orderVO.getId());
        orderDO.setAppid(appid);
        //生成唯一订单标识
        orderDO.setTradeNo(CommonUtils.generateUUID());

        orderDO.setState(orderVO.getState());
        orderDO.setTotalFee(orderVO.getTotal_fee());
        orderDO.setUserId(orderVO.getUser_id());
        orderDO.setNickName(orderVO.getNick_name());
        orderDO.setIcon(orderVO.getIcon());
        orderDO.setAddressId(orderVO.getAddress_id());
        //获取当前时间
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        orderDO.setCreateTime(sdf.format(new Date()));

        orderDO.setIp(orderVO.getIp());
        orderDO.setIsDeleted(0);
        orderDO.setOrderDetailList(OrderDetailDO.convertDOList(orderVO.getOrder_detail_list()));

        return orderDO;
    }


}
