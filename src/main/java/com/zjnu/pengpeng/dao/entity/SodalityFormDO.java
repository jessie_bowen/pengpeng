package com.zjnu.pengpeng.dao.entity;

import com.zjnu.pengpeng.controller.entity.SodalityFormVO;
import lombok.Data;

/**
 * @author ZhaoBW
 * @version 1.0
 * @date 2020/11/21 9:38
 */
@Data
public class SodalityFormDO {

    private Integer id;
    private Integer userId;
    private String academy;
    private String classes;
    private String name;
    private String studentNum;
    private String phone;
    private String wechatId;
    private String sex;
    private Integer activityId;
    private Integer isDeleted;

    public static SodalityFormDO convertDO(SodalityFormVO sodalityFormVO){
        SodalityFormDO sodalityFormDO = new SodalityFormDO();
        sodalityFormDO.setId(sodalityFormVO.getId());
        sodalityFormDO.setAcademy(sodalityFormVO.getAcademy());
        sodalityFormDO.setClasses(sodalityFormVO.getClasses());
        sodalityFormDO.setName(sodalityFormVO.getName());
        sodalityFormDO.setStudentNum(sodalityFormVO.getStudent_num());
        sodalityFormDO.setPhone(sodalityFormVO.getPhone());
        sodalityFormDO.setWechatId(sodalityFormVO.getWechat_id());
        sodalityFormDO.setSex(sodalityFormVO.getSex());
        sodalityFormDO.setUserId(sodalityFormVO.getUserId());
        sodalityFormDO.setActivityId(sodalityFormVO.getActivity_id());
        return sodalityFormDO;
    }


}
