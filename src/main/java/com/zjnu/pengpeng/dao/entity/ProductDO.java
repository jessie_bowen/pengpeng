package com.zjnu.pengpeng.dao.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.zjnu.pengpeng.controller.entity.ProductVO;
import lombok.Data;

import java.util.List;

/**
 * @author ZhaoBW
 * @version 1.0
 * @date 2020/11/8 20:42
 */
@Data
@TableName("product")
public class ProductDO {

    @TableId(type = IdType.AUTO)
    private Integer id;
    private String title;
    private String content;
    private Double price;
    private Double originalPrice;
    private String address;
    private String phone;
    private String classify;
    private String freshDegree;
    private String tradeType;
    private String time;
    private Integer sellerId;
    private String sellerNickName;
    private String sellerIcon;
    private Integer isPurchased;
    private Integer isFinished;
    private Integer viewCount;
    private Integer buyerId;
    private String buyerNickName;
    private String buyerIcon;
    private String buyerAddress;
    private String image;
    public static ProductDO convertDaoEntity(ProductVO viewProduct){
        ProductDO dataProduct = new ProductDO();
        dataProduct.setId(viewProduct.getId());
        dataProduct.setTitle(viewProduct.getTitle());
        dataProduct.setContent(viewProduct.getContent());
        dataProduct.setPrice(viewProduct.getPrice());
        dataProduct.setOriginalPrice(viewProduct.getOriginal_price());
        dataProduct.setAddress(viewProduct.getAddress());
        dataProduct.setPhone(viewProduct.getPhone());
        dataProduct.setClassify(viewProduct.getClassify());
        dataProduct.setFreshDegree(viewProduct.getFresh_degree());
        dataProduct.setTradeType(viewProduct.getTrade_type());
        dataProduct.setTime(viewProduct.getTime());
        dataProduct.setSellerId(viewProduct.getSeller_id());
        if(viewProduct.getIs_purchased() == null || !viewProduct.getIs_purchased()){
            dataProduct.setIsPurchased(0);
        }else{
            dataProduct.setIsPurchased(1);
        }
        if(viewProduct.getIs_finished() == null || !viewProduct.getIs_finished()){
            dataProduct.setIsFinished(0);
        }else{
            dataProduct.setIsFinished(1);
        }

        dataProduct.setViewCount(viewProduct.getView_count());
        dataProduct.setBuyerId(viewProduct.getBuyer_id());
        List<String> imgs = viewProduct.getImgs();

        //把图片数组转成字符串
        if(imgs != null && imgs.size() > 0){
            StringBuilder sb = new StringBuilder();
            for (String img : imgs) {
                sb.append(img).append(",");
            }
            String image = sb.toString();
            image = image.substring(0,image.length()-1);
            dataProduct.setImage(image);
        }

        return dataProduct;

    }


}
