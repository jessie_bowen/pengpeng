package com.zjnu.pengpeng.dao.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.zjnu.pengpeng.controller.entity.CommentVO;
import lombok.Data;

/**
 * @author ZhaoBW
 * @version 1.0
 * @date 2020/11/6 20:06
 */
@Data
@TableName("comment")
public class CommentDO {

    @TableId(type = IdType.AUTO)
    private Integer id;
    private Integer postId;
    private String content;
    private String time;
    private Integer userId;
    private String nickName;
    private String icon;
    private Integer replyUserId;
    private String replyUserNickName;
    private String replyUserIcon;

    public static CommentDO convertComment(CommentVO controllerEntity){
        CommentDO daoEntity = new CommentDO();
        daoEntity.setId(controllerEntity.getId());
        daoEntity.setPostId(controllerEntity.getPost_id());
        daoEntity.setContent(controllerEntity.getContent());
        daoEntity.setTime(controllerEntity.getTime());
        daoEntity.setUserId(controllerEntity.getUser_id());
        daoEntity.setNickName(controllerEntity.getNick_name());
        daoEntity.setIcon(controllerEntity.getIcon());
        daoEntity.setReplyUserId(controllerEntity.getReply_user_id());
        daoEntity.setReplyUserNickName(controllerEntity.getReply_user_nick_name());
        daoEntity.setReplyUserIcon(controllerEntity.getReply_user_icon());
        return daoEntity;
    }


}
