package com.zjnu.pengpeng.dao.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.List;

/**
 * @author ZhaoBW
 * @version 1.0
 * @date 2020/11/6 15:50
 */
@Data
@TableName("file")
public class FileDO {

    @TableId(type = IdType.AUTO)
    private Integer id;
    private String tableName;
    private Integer businessId;
    private String url;

}
