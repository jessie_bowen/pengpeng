package com.zjnu.pengpeng.dao.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.zjnu.pengpeng.controller.entity.UserVO;
import lombok.Data;

import java.util.List;


/**
 * @author zbw
 */
@Data
@TableName("user")
public class UserDO {
    @TableId(type = IdType.AUTO)
    private Integer id;
    private String userName;
    private String password;
    private String hobby;
    private String icon;
    private String phone;
    private String sex;
    private String address;
    private String focusUsers;
    private String isFocusedUsers;
    private String openid;
    private String nickName;

    public static UserDO convertToDaoEntity(UserVO userVO){
        UserDO entity = new UserDO();
        entity.setNickName(userVO.getNick_name());
        entity.setIcon(userVO.getIcon());
        entity.setSex(userVO.getSex());
        return entity;
    }



}
