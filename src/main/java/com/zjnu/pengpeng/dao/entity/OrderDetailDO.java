package com.zjnu.pengpeng.dao.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.zjnu.pengpeng.controller.entity.OrderDetailVO;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ZhaoBW
 * @version 1.0
 * @date 2020/11/19 9:29
 */
@Data
@TableName("order_detail")
public class OrderDetailDO {
    @TableId(type = IdType.AUTO)
    private Integer id;
    private Integer orderId;
    private Integer itemId;
    private String itemName;
    private String itemImage;
    private String itemPrice;
    /**
     * 规格字符串
     */
    private String specification;
    private Integer count;
    private Integer isDeleted;

    public static List<OrderDetailDO> convertDOList(List<OrderDetailVO> voList){
        List<OrderDetailDO> doList = new ArrayList<>(voList.size());
        for (OrderDetailVO vo : voList) {
            OrderDetailDO orderDetail = new OrderDetailDO();
            orderDetail.setId(vo.getId());
            orderDetail.setOrderId(vo.getOrder_id());
            orderDetail.setItemId(vo.getItem_id());
            orderDetail.setItemName(vo.getItem_name());
            orderDetail.setItemImage(vo.getItem_image());
            orderDetail.setItemPrice(vo.getItem_price());
            orderDetail.setSpecification(vo.getSpecification());
            orderDetail.setCount(vo.getCount());
            orderDetail.setIsDeleted(0);
            doList.add(orderDetail);
        }
        return doList;
    }


}
