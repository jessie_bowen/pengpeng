package com.zjnu.pengpeng.dao.mapper;

import com.zjnu.pengpeng.dao.entity.OrderDO;
import com.zjnu.pengpeng.dao.entity.OrderDetailDO;

/**
 * @author ZhaoBW
 * @version 1.0
 * @date 2020/11/18 18:04
 */
public interface OrderMapper {

    /**
     * 插入订单信息
     * @param order 订单详情
     * @return 返回订单主键id
     */
    int insertOrder(OrderDO order);

    /**
     * 插入与订单关联的商品信息
     * @param orderDetail 商品
     * @return 返回detail主键
     */
    int insertOrderDetail(OrderDetailDO orderDetail);

}
