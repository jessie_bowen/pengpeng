package com.zjnu.pengpeng.dao.mapper;

import com.zjnu.pengpeng.dao.entity.CartItemDO;

import java.util.List;

/**
 * @author ZhaoBW
 * @version 1.0
 * @date 2020/12/8 12:59
 */
public interface ShoppingCartMapper {

    /**
     * 添加购物车
     * @param cartItem 购物车item
     */
    void addCart(CartItemDO cartItem);

    /**
     * 增加购物车item的数量
     * @param id itemId
     */
    void increaseCartItemCount(int id);

    /**
     * 减少购物车item的数量
     * @param id itemId
     */
    void decreaseCartItemCount(int id);

    /**
     * 查询购物车item的数量
     * @param id itemId
     * @return count
     */
    int findCartItemCount(int id);

    /**
     * 删除购物车item
     * @param id itemId
     */
    void deleteCartItem(int id);

    /**
     * 根据用户id查询购物车
     * @param userId 用户id
     * @return 购物车item列表
     */
    List<CartItemDO> findCartItems(int userId);

}
