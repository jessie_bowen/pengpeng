package com.zjnu.pengpeng.dao.mapper;

import com.zjnu.pengpeng.dao.entity.ActivityDO;
import com.zjnu.pengpeng.dao.entity.SodalityFormDO;

import java.util.List;

/**
 * @author ZhaoBW
 * @version 1.0
 * @date 2020/11/21 9:56
 */
public interface SodalityFormMapper {

    /**
     * 插入申请表
     * @param formDO 表单
     * @return 数据库主键
     */
    int insertForm(SodalityFormDO formDO);

    /**
     * 查询申请表
     * @return 表单
     */
    List<SodalityFormDO> findSodalityForm();

    /**
     * 查询目前的活动
     * @return activity
     */
    ActivityDO getActivity();

}
