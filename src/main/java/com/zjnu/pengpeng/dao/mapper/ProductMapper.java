package com.zjnu.pengpeng.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zjnu.pengpeng.dao.entity.ProductDO;

import java.util.List;

/**
 * @author ZhaoBW
 * @version 1.0
 * @date 2020/11/8 20:47
 */
public interface ProductMapper extends BaseMapper<ProductDO> {

    /**
     * 插入商品
     * @param product 商品
     */
    void insertProduct(ProductDO product);

    /**
     * 查询所有商品
     * @return 商品列表
     */
    List<ProductDO> findAllProduct();

    /**
     * 购买商品
     * @param id 商品id
     * @param buyerId 买家id
     */
    void updateBuyerOfProduct(int id, int buyerId);

    /**
     * 取消购买商品
     * @param id 商品id
     */
    void cancelPurchasingProduct(int id);

    /**
     * 查询卖家商品
     * @param sellerId 卖家id
     * @return 商品列表
     */
    List<ProductDO> findProductBySellId(int sellerId);

    /**
     * 查询买家商品
     * @param buyerId 买家id
     * @return 商品列表
     */
    List<ProductDO> findProductByBuyerId(int buyerId);

    /**
     * 结束商品订单
     * @param id 商品id
     */
    void finishProduct(int id);

    /**
     * 根据商品id查询商品详情
     * @param id 商品id
     * @return 商品详情
     */
    ProductDO findProductById(int id);
}
