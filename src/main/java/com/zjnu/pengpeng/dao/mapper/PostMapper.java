package com.zjnu.pengpeng.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zjnu.pengpeng.dao.entity.PostDO;
import com.zjnu.pengpeng.dao.entity.PostLabelDO;

import java.util.List;

/**
 * @author ZhaoBW
 * @version 1.0
 * @date 2020/11/6 8:39
 */
public interface PostMapper extends BaseMapper<PostDO> {

    /**
     * 根据用户查询帖子
     * @param userId userId
     * @return 帖子列表
     */
    List<PostDO> findPostByUser(int userId);

    /**
     * 插入帖子
     * @param entity 帖子实体类
     * @return 插入帖子的id
     */
    void insertPost(PostDO entity);

    /**
     * 查询所有帖子标签
     * @return 帖子标签列表
     */
    List<PostLabelDO> findAllPostLabels();

    /**
     * 根据标签查询帖子
     * @param labelId 标签id
     * @return 帖子列表
     */
    List<PostDO> findPostByLabel(Integer labelId);

    /**
     * 更新帖子点赞数量
     * @param postId 帖子
     */
    void updatePraise(int postId);

    /**
     * 更新帖子数量数量
     * @param postId 帖子
     */
    void updateCommentCount(int postId);

    /**
     * 搜索帖子
     * @param content 内容
     * @return 帖子列表
     */
    List<PostDO> findPostByContent(String content);

    /**
     * 更新帖子状态
     * @param id
     */
    void updateLabel(Integer id);

}
