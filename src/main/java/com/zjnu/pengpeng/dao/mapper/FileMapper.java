package com.zjnu.pengpeng.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zjnu.pengpeng.dao.entity.FileDO;

/**
 * @author ZhaoBW
 * @version 1.0
 * @date 2020/11/6 15:50
 */
public interface FileMapper extends BaseMapper<FileDO> {
}
