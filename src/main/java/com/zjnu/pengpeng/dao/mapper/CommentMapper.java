package com.zjnu.pengpeng.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zjnu.pengpeng.dao.entity.CommentDO;

import java.util.List;

/**
 * @author ZhaoBW
 * @version 1.0
 * @date 2020/11/6 20:11
 */
public interface CommentMapper extends BaseMapper<CommentDO> {

    /**
     * 插入一条评论
     * @param entity 评论实例
     */
    void insertComment(CommentDO entity);

    /**
     * 根据帖子id查询评论
     * @param postId 帖子id
     * @return 评论列表
     */
    List<CommentDO> findCommentByPost(int postId);


}
