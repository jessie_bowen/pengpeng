package com.zjnu.pengpeng.dao.mapper;

import com.zjnu.pengpeng.dao.entity.FocusDO;
import com.zjnu.pengpeng.dao.entity.SodalityFormDO;
import com.zjnu.pengpeng.dao.entity.UserDO;
import java.util.List;


/**
 * @author zbw
 */
public interface UserMapper {


    /**
     * 根据openid获取用户
     * @param openid 微信平台用户唯一标识
     * @return UserDO
     */
    UserDO findUserByOpenid(String openid);

    /**
     * 根据用户名获取用户信息
     * @param userName 用户名
     * @return 用户信息
     */
    UserDO findUserByUserName(String userName);

    /**
     * 查询用户粉丝的id
     * @param userId 用户id
     * @return 粉丝id列表
     */
    List<Integer> findIdOfFansByUserId(Integer userId);

    /**
     * 获取用户关注的id
     * @param userId 用户id
     * @return 关注的id
     */
    List<Integer> findIdOfFocusByUserId(Integer userId);

    /**
     * 获取用户信息
     * @param id 用户id
     * @return 用户信息
     */
    UserDO findUserInfo(int id);

    /**
     * 找回密码
     * @param userName 用户名
     * @return 密码
     */
    String findPwdByName(String userName);

    /**
     * 关注用户
     * @param userId 用户id
     * @param focusUserId 关注用户id
     * @param isFocusEachOther 是否互相关注
     */
    void insertFocus(int userId, int focusUserId, int isFocusEachOther);

    /**
     * 查询是否关注
     * @param userId 用户id
     * @param focusUserId 关注用户id
     * @return 关注表
     */
    FocusDO isFocus(int userId, int focusUserId);

    /**
     * 修改关注表中，是否互相关注状态
     * @param id 关注表id
     * @param isFocusEachOther 是否互相关注
     */
    void updateFocus(int id,int isFocusEachOther);

    /**
     * 删除关注
     * @param userId 用户id
     * @param focusUserId 关注用户id
     */
    void deleteFocus(int userId,int focusUserId);

    /**
     * 获取所有爱好的标签
     * @return 标签列表
     */
    List<String> findAllCharacters();

    /**
     * 插入用户爱好
     * @param userId 用户id
     * @param hobby 爱好标签
     */
    void updateUserCharacter(int userId, String hobby);

    /**
     * 删除用户的爱好标签
     * @param userId 用户id
     */
    void deleteUserCharacter(int userId);

    /**
     * 获取用户的粉丝
     * @param userId userId
     * @return 用户列表
     */
    List<UserDO> findFansByUserId(int userId);

    /**
     * 获取用户关注的人
     * @param userId 用户id
     * @return 关注人列表
     */
    List<UserDO> findFocusUsers(int userId);

    /**
     * 搜索用户
     * @param userName 用户id
     * @return 搜索结果
     */
    List<UserDO> searchUser(String userName);

    /**
     * 更新地址
     * @param id user id
     * @param address 地址
     */
    void updateAddress(int id, String address);


    /**
     * 插入用户
     * @param userDO user
     */
    void insertUser(UserDO userDO);

    /**
     * 修改用户认证状态
     * @param academy 学院
     * @param major 专业
     * @param studentNum 学号
     * @param name 姓名
     * @param cardUrl 学生卡地址
     * @param id user_id
     */
    void authenticateUser(String academy, String major, String studentNum,
                          String name, String cardUrl,Integer id);

    /**
     * 获取用户认证状态
     * @param id 用户id
     * @return 认证状态
     */
    Integer getAuthenticationState(Integer id);

    /**
     * 查询用户是否已经报名
     * @param userId 用户id
     * @param activityId 活动id
     * @return 报名表id
     */
    Integer findFormByUserAndActivity(Integer userId, Integer activityId);
}
