package com.zjnu.pengpeng.dao.mapper;

import com.zjnu.pengpeng.dao.entity.BannerDO;
import java.util.List;

/**
 * @author ZhaoBW
 * @version 1.0
 * @date 2020/11/6 14:55
 */
public interface BannerMapper {

    /**
     * 查询所有轮播图
     * @return 轮播图
     */
    List<BannerDO> findAllBanner();



}
