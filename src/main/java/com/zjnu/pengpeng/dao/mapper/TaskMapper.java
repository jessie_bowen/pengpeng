package com.zjnu.pengpeng.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zjnu.pengpeng.dao.entity.TaskDO;

import java.util.List;

/**
 * @author ZhaoBW
 * @version 1.0
 * @date 2020/11/8 15:40
 */
public interface TaskMapper extends BaseMapper<TaskDO> {

    /**
     * 插入一条任务
     * @param task
     */
    void insertTask(TaskDO task);

    /**
     * 查询所有任务
     * @return 任务列表
     */
    List<TaskDO> findAllTask();

    /**
     * 接收任务
     * @param taskId 任务id
     * @param receiverId 接收用户id
     */
    void receiveTask(int taskId, int receiverId);

    /**
     * 完成任务
     * @param taskId 任务id
     */
    void finishTask(int taskId);

    /**
     * 结束任务
     * @param taskId 任务id
     */
    void endTask(int taskId);

    /**
     * 取消接收任务
     * @param taskId 任务id
     */
    void cancelTask(int taskId);

    /**
     * 根据userId查询任务
     * @param userId userId
     * @return 任务列表
     */
    List<TaskDO> findTaskByUserId(int userId);

    /**
     * 根据receiverId查询任务
     * @param receiverId receiverId
     * @return 任务列表
     */
    List<TaskDO> findTaskByReceiverId(int receiverId);

    /**
     * 根据id查询任务
     * @param id 任务id
     * @return 任务详情
     */
    TaskDO findTaskById(int id);

    /**
     * 根据任务状态查询任务
     * @param state
     * @return
     */
    List<TaskDO> findTaskByState(int state);
}
