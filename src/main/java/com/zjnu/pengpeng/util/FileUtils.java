package com.zjnu.pengpeng.util;

import com.github.tobato.fastdfs.domain.StorePath;
import com.github.tobato.fastdfs.service.FastFileStorageClient;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.ClassUtils;
import org.springframework.web.multipart.MultipartFile;
import sun.misc.BASE64Decoder;

import java.io.*;
import java.util.Random;

/**
 * @author zbw
 */
@Component
public class FileUtils {

    @Autowired
    FastFileStorageClient storageClient;

    public String uploadBase64Image(String imageStr){
        BASE64Decoder decoder = new BASE64Decoder();
        try {
            //Base64解码
            byte[] b = decoder.decodeBuffer(imageStr);
            for (int i = 0; i < b.length; ++i) {
                //调整异常数据
                if (b[i] < 0) {
                    b[i] += 256;
                }
            }
            //生成jpeg图片
            String path = ClassUtils.getDefaultClassLoader().getResource("").getPath();
            String file = new Random().nextInt(99999999) + ".jpg";

            String filePath = path + file;
            //新生成的图片
            OutputStream out = new FileOutputStream(filePath);
            out.write(b);
            out.flush();
            out.close();
            File file2 = new File(filePath);
            StorePath storePath = storageClient.uploadFile(null, new FileInputStream(file2), file2.length(), "png");
            return storePath.getGroup() + "/" + storePath.getPath();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public String uploadFile(MultipartFile source){
        InputStream is = null;
        try
        {

            // 获取文件流
            is = source.getInputStream();
            // 进行文件上传
            StorePath storePath = storageClient.uploadFile(is, source.getSize(), FilenameUtils.getExtension(source.getOriginalFilename()), null);
            // 获得文件上传后访问地址
            String fullPath = storePath.getFullPath();
            // 打印访问地址
            return  fullPath;
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        finally
        {
            try
            {
                if(is != null)
                {
                    // 关闭流资源
                    is.close();
                }
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }
        return null;
    }


}
