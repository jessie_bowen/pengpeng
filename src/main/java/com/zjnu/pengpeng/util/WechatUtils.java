package com.zjnu.pengpeng.util;

import com.alibaba.fastjson.JSONObject;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;
import java.util.SortedMap;

/**
 * @author ZhaoBW
 * @version 1.0
 * @date 2020/11/19 14:30
 */
@Component
public class WechatUtils {


    static RedisUtil redisUtil;

    /**
     * 解决静态方法中引用redisUtil
     * @param redisUtil Spring注入的redisUtil
     */
    @Autowired
    public WechatUtils(RedisUtil redisUtil){
        WechatUtils.redisUtil = redisUtil;
    }

    /**
     * XML格式字符串转换为Map
     *
     * @param strXML XML字符串
     * @return XML数据转换后的Map
     * @throws Exception
     */
    public static Map<String, String> xmlToMap(String strXML) throws Exception {
        try {
            Map<String, String> data = new HashMap<String, String>();
            DocumentBuilder documentBuilder = WXPayXmlUtil.newDocumentBuilder();
            InputStream stream = new ByteArrayInputStream(strXML.getBytes("UTF-8"));
            org.w3c.dom.Document doc = documentBuilder.parse(stream);
            doc.getDocumentElement().normalize();
            NodeList nodeList = doc.getDocumentElement().getChildNodes();
            for (int idx = 0; idx < nodeList.getLength(); ++idx) {
                Node node = nodeList.item(idx);
                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    org.w3c.dom.Element element = (org.w3c.dom.Element) node;
                    data.put(element.getNodeName(), element.getTextContent());
                }
            }
            try {
                stream.close();
            } catch (Exception ex) {
                // do nothing
            }
            return data;
        } catch (Exception ex) {
            throw ex;
        }

    }

    /**
     * 将Map转换为XML格式的字符串
     *
     * @param data Map类型数据
     * @return XML格式的字符串
     * @throws Exception
     */
    public static String mapToXml(Map<String, String> data) throws Exception {
        org.w3c.dom.Document document = WXPayXmlUtil.newDocument();
        org.w3c.dom.Element root = document.createElement("xml");
        document.appendChild(root);
        for (String key: data.keySet()) {
            String value = data.get(key);
            if (value == null) {
                value = "";
            }
            value = value.trim();
            org.w3c.dom.Element filed = document.createElement(key);
            filed.appendChild(document.createTextNode(value));
            root.appendChild(filed);
        }
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer transformer = tf.newTransformer();
        DOMSource source = new DOMSource(document);
        transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        StringWriter writer = new StringWriter();
        StreamResult result = new StreamResult(writer);
        transformer.transform(source, result);
        String output = writer.getBuffer().toString();
        try {
            writer.close();
        }
        catch (Exception ex) {
        }
        return output;
    }

    /**
     * 生成微信支付签名，步骤参见微信支付开发文档中，“API列表-->统一下单-->sign参数”
     * @param params 参数
     * @param secretKey 微信密钥
     * @return 签名字符串
     */
    public static String createSign(SortedMap<String, String> params, String secretKey) throws NoSuchAlgorithmException {
        //1、对参数按照key=value的格式，并按照参数名ASCII字典序排序如下：
        //stringA="appid=wxd930ea5d5a258f4f&body=test&device_info=1000&mch_id=10000100&nonce_str=ibuaiVcKdpRxkhJA";
        StringBuilder sb = new StringBuilder();
        for(Map.Entry<String,String> entry: params.entrySet()){
            String key = entry.getKey();
            String value = entry.getValue();
            if(!StringUtils.isEmpty(value)){
                sb.append(key + "=" + value + "&");
            }
        }

        //2、拼接API密钥：stringSignTemp=stringA+"&key=192006250b4c09247ec02edce69f6a2d"
        // 注：key为商户平台设置的密钥key
        sb.append("key=").append(secretKey);

        //3、将拼接好的字符串，用MD5加密
        return CommonUtils.MD5(sb.toString());
    }


    /**
     * 校验签名
     * @param params 校验参数
     * @param secretKey 密钥
     * @return 校验结果
     */
    public static boolean verifySign(SortedMap<String, String> params, String secretKey) throws NoSuchAlgorithmException {

        String sign = createSign(params,secretKey);


        String wxPaySign = params.get("sign").toUpperCase();
        return sign.equals(wxPaySign);
    }

    /**
     * 获取微信平台用户唯一标识openid
     * @param code 授权码
     * @return openid
     */
    public static String getOpenid(String code){
        String url = "https://api.weixin.qq.com/sns/jscode2session";
        Map<String,String> param = new HashMap<>(4);
        param.put("appid","wx1f58fcb342a8478b");
        param.put("secret","09425a8d3006911cdf3b69217f9bff4d");
        param.put("js_code",code);
        param.put("grant_type","authorization_code");
        String finalUrl = HttpUtils.generateGetParam(url,param);
        Map<String, Object> wechatResponse = HttpUtils.doGet(finalUrl);
        if(!wechatResponse.isEmpty()){
            return (String) wechatResponse.get("openid");

        }
        return "";
    }

    /**
     * 获取微信平台access_token
     * @return
     */
    public static String getAccessToken(){
        String wechat_token = (String) redisUtil.get("wechat_token");

        if(StringUtils.isEmpty(wechat_token)){
            String url = "https://api.weixin.qq.com/cgi-bin/token";
            Map<String,String> param = new HashMap<>(3);
            param.put("grant_type","client_credential");
            param.put("appid","wx1f58fcb342a8478b");
            param.put("secret","09425a8d3006911cdf3b69217f9bff4d");
            String finalUrl = HttpUtils.generateGetParam(url,param);
            Map<String, Object> wechatResponse = HttpUtils.doGet(finalUrl);
            if(!wechatResponse.isEmpty()){
                wechat_token = (String) wechatResponse.get("access_token");
                redisUtil.set("wechat_token",wechat_token,7100);
            }
        }
        return wechat_token;
    }

    public static boolean checkContent(String content) throws IOException {
        String access_token = getAccessToken();
        String url = "https://api.weixin.qq.com/wxa/msg_sec_check?access_token=" + access_token;
        //创建客户端
        HttpClient httpclient = HttpClients.createDefault();
        //创建一个post请求
        HttpPost request = new HttpPost(url);
        //设置响应头
        request.setHeader("Content-Type", "application/json;charset=UTF-8");
        //通过fastJson设置json数据
        JSONObject postData = new JSONObject();
        //设置要检测的内容
        postData.put("content", content);
        String jsonString = postData.toString();
        request.setEntity(new StringEntity(jsonString,"utf-8"));
        HttpResponse response = httpclient.execute(request);
        // 从响应模型中获取响应实体
        HttpEntity entity = response.getEntity();
        //得到响应结果
        String result = EntityUtils.toString(entity,"utf-8");
        //打印检测结果
        //将响应结果变成json
        JSONObject resultJsonObject = JSONObject.parseObject(result);

        String errCode =resultJsonObject.getString("errcode");
        //当content内含有敏感信息，则返回87014
        if(errCode.equals("87014")){
            return false;
        }
        return true;
    }

    /**
     * 检测图片是否含有违法违规内容
     * 频率限制:单个appid调用上限为1000次/分钟，100,000次/天
     * @param inputStream 图片文件  流multipartFile.getInputStream()
     * @param contentType 图片文件类型  multipartFile.getContentType()
     * @return true:含敏感信息   false：正常
     * media 要检测的图片文件，格式支持PNGJPEGJPGGIF, 像素不超过750x1334
     */
    public static Boolean checkImg(InputStream inputStream, String contentType){
        try {
            //获取token  小程序全局唯一后台接口调用凭据
            String accessToken = getAccessToken();
            CloseableHttpClient httpclient = HttpClients.createDefault();
            CloseableHttpResponse response;
            HttpPost request = new HttpPost("https://api.weixin.qq.com/wxa/img_sec_check?access_token=" + accessToken);
            request.addHeader("Content-Type", "application/octet-stream");
            byte[] byt = new byte[inputStream.available()];
            inputStream.read(byt);
            request.setEntity(new ByteArrayEntity(byt, ContentType.create(contentType)));
            response = httpclient.execute(request);
            HttpEntity httpEntity = response.getEntity();
            // 转成string
            String result = EntityUtils.toString(httpEntity, "UTF-8");
            System.out.println(result);
            JSONObject jso = JSONObject.parseObject(result);

            String errcode = jso.getString("errcode");
            //当content内含有敏感信息，则返回87014
            return "87014".equals(errcode);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

}
