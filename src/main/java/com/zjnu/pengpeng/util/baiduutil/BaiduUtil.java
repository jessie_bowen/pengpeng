package com.zjnu.pengpeng.util.baiduutil;

import com.alibaba.fastjson.JSONObject;
import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;
import com.zjnu.pengpeng.util.RedisUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.Map;

/**
 * @author ZhaoBW
 * @version 1.0
 * @date 2020/12/7 13:52
 */
@Component
public class BaiduUtil {


    static RedisUtil redisUtil;

    @Autowired
    public BaiduUtil(RedisUtil redisUtil){
        BaiduUtil.redisUtil = redisUtil;
    }

    /**
     * 获取权限token
     * @return 返回示例：
     * {
     * "access_token": "24.460da4889caad24cccdb1fea17221975.2592000.1491995545.282335-1234567",
     * "expires_in": 2592000
     * }
     */
    public static String getAuth() {
        //先去redis中获取token，若为空，再去百度拿
        String auth = (String) redisUtil.get("ocr_token");
        if(StringUtils.isEmpty(auth)){
            // 官网获取的 API Key 更新为你注册的
            String clientId = "sMv9foU6RUeOKY8kmKwgR9Cr";
            // 官网获取的 Secret Key 更新为你注册的
            String clientSecret = "EClGrG57BQTdsISpYPehPh88NuwLd3T4";
            auth = getAuth(clientId, clientSecret);
            redisUtil.set("ocr_token",auth,2591900);
        }
        return auth;
    }

    /**
     * 获取API访问token
     * 该token有一定的有效期，需要自行管理，当失效时需重新获取.
     * @param ak - 百度云官网获取的 API Key
     * @param sk - 百度云官网获取的 Securet Key
     * @return assess_token 示例：
     * "24.460da4889caad24cccdb1fea17221975.2592000.1491995545.282335-1234567"
     */
    public static String getAuth(String ak, String sk) {
        // 获取token地址
        String authHost = "https://aip.baidubce.com/oauth/2.0/token?";
        String getAccessTokenUrl = authHost
                // 1. grant_type为固定参数
                + "grant_type=client_credentials"
                // 2. 官网获取的 API Key
                + "&client_id=" + ak
                // 3. 官网获取的 Secret Key
                + "&client_secret=" + sk;
        try {
            URL realUrl = new URL(getAccessTokenUrl);
            // 打开和URL之间的连接
            HttpURLConnection connection = (HttpURLConnection) realUrl.openConnection();
            connection.setRequestMethod("GET");
            connection.connect();
            // 获取所有响应头字段
            Map<String, List<String>> map = connection.getHeaderFields();

            // 定义 BufferedReader输入流来读取URL的响应
            BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String result = "";
            String line;
            while ((line = in.readLine()) != null) {
                result += line;
            }
            /**
             * 返回结果示例
             */
            Gson gson = new Gson();
            LinkedTreeMap resultMap = gson.fromJson(result,LinkedTreeMap.class);
            return (String) resultMap.get("access_token");
        } catch (Exception e) {
            System.err.printf("获取token失败！");
            e.printStackTrace(System.err);
        }
        return null;
    }

}
