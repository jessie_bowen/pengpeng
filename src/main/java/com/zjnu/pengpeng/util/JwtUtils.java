package com.zjnu.pengpeng.util;

import com.zjnu.pengpeng.controller.entity.UserVO;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import java.util.Calendar;
import java.util.Date;

/**
 * Jwt工具类
 * @author zbw
 */
public class JwtUtils {

    private static final String SUBJECT = "pengpeng";

    /**
     * 过期时间：半个月
     */
    //public static long EXPIRE = 1000*60*60*24*15;

    /**
     * 过期时间：2分钟
     */
    //public static long EXPIRE_TEST = 1000*60*2;


    /**
     * 碰碰密钥
     */
    private static final String PENGPENG_APP_SECRET = "pengpeng666";

    /**
     * 加密算法，生成JWT
     * @param userVO user信息
     * @return token串
     */
    public static String generateWebToken(UserVO userVO){
        //获取当前时间三个月以后的日期
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.MONTH,3);
        Date date = calendar.getTime();

        String token = Jwts.builder().setSubject(SUBJECT)
                .claim("id", userVO.getId())
                .claim("nickName", userVO.getNick_name())
                .claim("sex", userVO.getSex())
                .claim("icon", userVO.getIcon())
                .setIssuedAt(new Date())
                .setExpiration(date)
                .signWith(SignatureAlgorithm.HS256,PENGPENG_APP_SECRET).compact();
        return token;
    }

    /**
     * 校验token
     * @param token token串
     * @return 用户信息
     */
    public static Claims checkJWT(String token){
        try{
            Claims claims = Jwts.parser().setSigningKey(PENGPENG_APP_SECRET).parseClaimsJws(token).getBody();
            return claims;
        }catch (Exception e){
            throw e;
        }
    }

}
