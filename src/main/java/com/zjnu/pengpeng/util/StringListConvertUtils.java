package com.zjnu.pengpeng.util;

import org.springframework.util.StringUtils;
import java.util.Arrays;
import java.util.List;

/**
 * @author ZhaoBW
 * @version 1.0
 * @date 2020/11/26 15:20
 */
public class StringListConvertUtils {

    public static String listToString(List<String> list){
        if(list == null || list.size() == 0){
            return "";
        }
        StringBuilder sb = new StringBuilder();
        for (String character : list) {
            sb.append(character).append(",");
        }
        String str = sb.toString();
        return str.substring(0,str.length()-1);
    }

    public static String integerListToString(List<Integer> list){
        if(list == null || list.size() == 0){
            return "";
        }
        StringBuilder sb = new StringBuilder();
        for (Integer item : list) {
            sb.append(item).append(",");
        }
        String str = sb.toString();
        return str.substring(0,str.length()-1);
    }

    public static List<String> stringToList(String str){
        if(StringUtils.isEmpty(str)){
            return null;
        }
        String[] split = str.split(",");
        return Arrays.asList(split);
    }

}
