package com.zjnu.pengpeng.service;

import com.zjnu.pengpeng.controller.entity.PostVO;
import com.zjnu.pengpeng.dao.entity.PostDO;
import com.zjnu.pengpeng.dao.entity.PostLabelDO;

import java.io.IOException;
import java.util.List;

/**
 * @author ZhaoBW
 * @version 1.0
 * @date 2020/11/6 8:54
 */
public interface PostService {

    /**
     * 根据用户查询帖子
     * @param userId 用户id
     * @param pageIndex 页数
     * @param pageSize 容量
     * @return 帖子
     */
    List<PostDO> findPostByService(int userId, int pageIndex, int pageSize);

    /**
     * 发布帖子
     * @param controllerEntity 帖子内容
     * @throws IOException IO异常
     */
    void uploadPost(PostVO controllerEntity) throws IOException;

    /**
     * 查询所有帖子标签
     * @return 标签列表
     */
    List<PostLabelDO> findAllPostLabels();

    /**
     * 根据标签查询帖子
     * @param labelId 标签id
     * @param pageNum 页数
     *  @param pageSize 容量
     * @return 帖子列表
     */
    List<PostDO> findPostByLabel(Integer labelId, int pageNum, int pageSize);

    /**
     * 点赞帖子
     * @param postId 帖子
     */
    void updatePraise(int postId);


    /**
     * 搜索帖子
     * @param content 内容
     * @param pageNum 页数
     *  @param pageSize 容量
     * @return 帖子列表
     */
    List<PostDO> findPostByContent(String content, int pageNum, int pageSize);
}
