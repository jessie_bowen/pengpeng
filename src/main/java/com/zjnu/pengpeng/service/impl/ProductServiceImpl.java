package com.zjnu.pengpeng.service.impl;

import com.github.pagehelper.PageHelper;
import com.zjnu.pengpeng.controller.entity.ProductVO;
import com.zjnu.pengpeng.dao.entity.ProductDO;
import com.zjnu.pengpeng.dao.mapper.FileMapper;
import com.zjnu.pengpeng.dao.mapper.ProductMapper;
import com.zjnu.pengpeng.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author ZhaoBW
 * @version 1.0
 * @date 2020/11/8 21:33
 */
@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    ProductMapper productMapper;

    @Autowired
    FileMapper fileMapper;

    @Override
    public void publishProduct(ProductVO viewProduct) {
        ProductDO daoProduct = ProductDO.convertDaoEntity(viewProduct);
        productMapper.insertProduct(daoProduct);
    }

    @Override
    public List<ProductDO> findAllProduct(int pageNum, int pageSize) {
        PageHelper.startPage(pageNum,pageSize);
        return productMapper.findAllProduct();
    }

    @Override
    public void purchaseProduct(int id, int buyerId) {
        productMapper.updateBuyerOfProduct(id,buyerId);
    }

    @Override
    public void cancelPurchasingProduct(int id) {
        productMapper.cancelPurchasingProduct(id);
    }

    @Override
    public List<ProductDO> findProductBySellId(int sellerId,int pageNum, int pageSize) {
        PageHelper.startPage(pageNum,pageSize);
        return productMapper.findProductBySellId(sellerId);
    }

    @Override
    public List<ProductDO> findProductByBuyerId(int buyerId,int pageNum, int pageSize) {
        PageHelper.startPage(pageNum,pageSize);
        return productMapper.findProductByBuyerId(buyerId);
    }

    @Override
    public void finishProduct(int id) {
        productMapper.finishProduct(id);
    }


}
