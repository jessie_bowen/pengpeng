package com.zjnu.pengpeng.service.impl;


import com.zjnu.pengpeng.dao.entity.CartItemDO;
import com.zjnu.pengpeng.dao.entity.ItemDO;
import com.zjnu.pengpeng.dao.mapper.ShoppingCartMapper;
import com.zjnu.pengpeng.interceptor.CustomException;
import com.zjnu.pengpeng.service.ShopService;
import com.zjnu.pengpeng.service.ShoppingCartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * @author ZhaoBW
 * @version 1.0
 * @date 2020/12/7 19:07
 */
@Service
@CacheConfig(cacheNames = "shoppingCartCache")
public class ShoppingCartServiceImpl implements ShoppingCartService {

    @Autowired
    ShoppingCartMapper cartMapper;

    @Autowired
    ShopService shopService;

    @Override
    public int addCart(int userId,int itemId) {
        ItemDO itemDO = shopService.findItemById(itemId);
        if(itemDO == null){
            throw new CustomException("商品不存在");
        }

        CartItemDO cartItemDO = CartItemDO.convertCartItem(userId,itemDO);
        cartMapper.addCart(cartItemDO);
        return cartItemDO.getId();
    }

    @Override
    public void increaseCartItemCount(int id) {
        cartMapper.increaseCartItemCount(id);
    }

    @Override
    public void decreaseCartItemCount(int id) {
        int count = cartMapper.findCartItemCount(id);
        if(count <= 1){
            cartMapper.deleteCartItem(id);
        }else{
            cartMapper.decreaseCartItemCount(id);
        }
    }

    @Override
    public List<CartItemDO> findCartItems(int userId) {
        return cartMapper.findCartItems(userId);
    }

    @Override
    public void deleteCartItem(int id) {
        cartMapper.deleteCartItem(id);
    }

}
