package com.zjnu.pengpeng.service.impl;

import com.github.pagehelper.PageHelper;
import com.zjnu.pengpeng.controller.entity.SodalityFormVO;
import com.zjnu.pengpeng.dao.entity.ActivityDO;
import com.zjnu.pengpeng.dao.entity.SodalityFormDO;
import com.zjnu.pengpeng.dao.mapper.SodalityFormMapper;
import com.zjnu.pengpeng.service.SodalityFormService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author ZhaoBW
 * @version 1.0
 * @date 2020/11/21 10:03
 */
@Service
public class SodalityFormServiceImpl implements SodalityFormService {

    @Autowired
    SodalityFormMapper sodalityFormMapper;

    @Override
    public int insertForm(SodalityFormVO form) {
        SodalityFormDO sodalityFormDO = SodalityFormDO.convertDO(form);
        sodalityFormMapper.insertForm(sodalityFormDO);
        return sodalityFormDO.getId();
    }

    @Override
    public List<SodalityFormVO> findSodalityForm(int pageNum, int pageSize) {
        PageHelper.startPage(pageNum,pageSize);
        List<SodalityFormDO> sodalityForm = sodalityFormMapper.findSodalityForm();
        return SodalityFormVO.convertVOList(sodalityForm);
    }

    @Override
    public ActivityDO getActivity() {
        return sodalityFormMapper.getActivity();
    }
}
