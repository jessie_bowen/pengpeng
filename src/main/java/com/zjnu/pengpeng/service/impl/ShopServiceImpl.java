package com.zjnu.pengpeng.service.impl;

import com.github.pagehelper.PageHelper;
import com.zjnu.pengpeng.dao.entity.BriefItemDO;
import com.zjnu.pengpeng.dao.entity.CategoryDO;
import com.zjnu.pengpeng.dao.entity.ItemDO;
import com.zjnu.pengpeng.dao.mapper.ShopMapper;
import com.zjnu.pengpeng.dao.mapper.ShoppingCartMapper;
import com.zjnu.pengpeng.service.ShopService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import java.util.List;

/**
 * @author ZhaoBW
 * @version 1.0
 * @date 2020/12/7 19:07
 */
@Service
@CacheConfig(cacheNames = "shopCache")
public class ShopServiceImpl implements ShopService {

    @Autowired
    ShopMapper shopMapper;

    @Autowired
    ShoppingCartMapper cartMapper;


    @Cacheable(value = "shopCategory", key = "#shopId")
    @Override
    public List<CategoryDO> findCategoryByShopId(int shopId) {
        return shopMapper.findCategoryByShopId(shopId);
    }

    @Cacheable(value = "briefItems", key = "#categoryId+'-'+#pageNum+'-'+#pageSize")
    @Override
    public List<BriefItemDO> findItemByCategory(int categoryId, int pageNum, int pageSize) {
        PageHelper.startPage(pageNum,pageSize);
        return shopMapper.findItemByCategory(categoryId);
    }

    @Cacheable(value = "item", key = "#id")
    @Override
    public ItemDO findItemById(int id) {
        return shopMapper.findItemById(id);
    }

    @Cacheable(value = "shopItem",key = "#shopId+'-'+#pageNum+'-'+#pageSize")
    @Override
    public List<ItemDO> findItemByShop(int shopId, int pageNum, int pageSize) {
        PageHelper.startPage(pageNum,pageSize);
        return shopMapper.findItemByShop(shopId);
    }

}
