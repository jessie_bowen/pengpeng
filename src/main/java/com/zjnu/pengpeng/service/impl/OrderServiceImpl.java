package com.zjnu.pengpeng.service.impl;

import com.zjnu.pengpeng.config.WeChatConfig;
import com.zjnu.pengpeng.controller.entity.OrderVO;
import com.zjnu.pengpeng.controller.entity.WechatPayParameterVO;
import com.zjnu.pengpeng.dao.entity.OrderDO;
import com.zjnu.pengpeng.dao.entity.OrderDetailDO;
import com.zjnu.pengpeng.dao.mapper.OrderMapper;
import com.zjnu.pengpeng.service.OrderService;
import com.zjnu.pengpeng.util.CommonUtils;
import com.zjnu.pengpeng.util.HttpUtils;
import com.zjnu.pengpeng.util.WechatUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 * @author ZhaoBW
 * @version 1.0
 * @date 2020/11/19 9:51
 */
@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    OrderMapper orderMapper;

    @Autowired
    WeChatConfig weChatConfig;

    @Override
    public WechatPayParameterVO createOrder(OrderVO orderVO) throws Exception {
        //1、生成订单，并插入数据库
        OrderDO orderDO = OrderDO.convertDO(orderVO,weChatConfig.getAppId());
        orderMapper.insertOrder(orderDO);
        List<OrderDetailDO> doList = orderDO.getOrderDetailList();
        for (OrderDetailDO orderDetailDO : doList) {
            orderDetailDO.setOrderId(orderDO.getId());
            orderMapper.insertOrderDetail(orderDetailDO);
        }
        //2、统一下单
        return unifiedOrder(orderDO);
    }

    /**
     * 统一下单方法
     */
    private WechatPayParameterVO unifiedOrder(OrderDO orderDO) throws Exception {
        //1、封装参数
        SortedMap<String,String> params = new TreeMap<>();
        params.put("appid",weChatConfig.getAppId());
        params.put("mch_id", weChatConfig.getMchId());
        params.put("nonce_str", CommonUtils.generateUUID());
        params.put("body","碰碰校园-订单支付");
        params.put("out_trade_no",orderDO.getTradeNo());
        params.put("total_fee",String.valueOf(orderDO.getTotalFee()));
        params.put("spbill_create_ip",orderDO.getIp());
        params.put("notify_url",weChatConfig.getPayCallbackUrl());
        params.put("trade_type","JSAPI");
        params.put("openid","o42wf5MXwW9vB12B-zz_TTTCTp8Q");

        //sign签名
        String sign = WechatUtils.createSign(params, weChatConfig.getKey());
        params.put("sign",sign);

        //map转xml
        String payXml = WechatUtils.mapToXml(params);
        System.out.println(payXml);

        //2、统一下单
        String response = HttpUtils.doPost(WeChatConfig.UNIFIED_ORDER_URL,payXml,5000);
        if(StringUtils.isEmpty(response)){
            throw new RuntimeException("调用微信统一下单失败");
        }
        Map<String, String> responseMap = WechatUtils.xmlToMap(response);
        System.out.println(responseMap);

        //3、生成paySign
        SortedMap<String,String> paySignParams = new TreeMap<>();
        paySignParams.put("appId",weChatConfig.getAppId());
        paySignParams.put("timeStamp",String.valueOf(System.currentTimeMillis()));
        paySignParams.put("nonceStr",CommonUtils.generateUUID());
        paySignParams.put("package","prepay_id="+responseMap.get("prepay_id"));
        paySignParams.put("signType","MD5");

        String paySign = WechatUtils.createSign(paySignParams, weChatConfig.getKey());

        //4、封装结果
        WechatPayParameterVO parameterVO = new WechatPayParameterVO();
        parameterVO.setPaySign(paySign);
        parameterVO.setTimeStamp(paySignParams.get("timeStamp"));
        parameterVO.setNonceStr(paySignParams.get("nonceStr"));
        parameterVO.setPackages(paySignParams.get("package"));
        parameterVO.setSignType(paySignParams.get("signType"));
        return parameterVO;
    }

}
