package com.zjnu.pengpeng.service.impl;


import com.zjnu.pengpeng.dao.entity.BannerDO;
import com.zjnu.pengpeng.dao.mapper.BannerMapper;
import com.zjnu.pengpeng.service.BannerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author ZhaoBW
 * @version 1.0
 * @date 2020/11/6 15:01
 */
@Service
public class BannerServiceImpl implements BannerService {

    @Autowired
    BannerMapper mapper;

    @Override
    public List<BannerDO> findAllBanner() {
        return mapper.findAllBanner();
    }


}
