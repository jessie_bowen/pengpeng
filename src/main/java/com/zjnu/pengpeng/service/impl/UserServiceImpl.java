package com.zjnu.pengpeng.service.impl;

import com.github.pagehelper.PageHelper;
import com.zjnu.pengpeng.controller.entity.UserVO;
import com.zjnu.pengpeng.dao.entity.FocusDO;
import com.zjnu.pengpeng.dao.entity.UserDO;
import com.zjnu.pengpeng.dao.mapper.UserMapper;
import com.zjnu.pengpeng.interceptor.CustomException;
import com.zjnu.pengpeng.service.UserService;
import com.zjnu.pengpeng.util.FileUtils;
import com.zjnu.pengpeng.util.StringListConvertUtils;
import com.zjnu.pengpeng.util.WechatUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author zbw
 */
@Service
public class UserServiceImpl implements UserService {
    @Autowired
    UserMapper mapper;

    @Autowired
    FileUtils fileUtils;

    @Override
    public UserDO login(String code, String nickName, String sex, String icon) {
        //1、获取openid
        String openid = WechatUtils.getOpenid(code);
        //2、判断openid是否已经存在
        UserDO userDO = mapper.findUserByOpenid(openid);
        //3、如果存在的话，获取用户信息
        if(userDO != null){
            //转换用户爱好、用户粉丝、关注用户列表
            convertUserProperties(userDO);
            return userDO;
        }else {
            //4、如果不存在，插入一条用户信息
            UserDO user = new UserDO();
            user.setNickName(nickName);
            user.setSex(sex);
            user.setIcon(icon);
            user.setOpenid(openid);
            mapper.insertUser(user);
            return user;
        }
    }

    /**
     * 拆分查找用户方法，并设置用户属性
     * 把一条复杂的sql语句，转换成几条简单的语句
     * @param userDO user
     */
    private void convertUserProperties(UserDO userDO){
        List<Integer> idOfFans = mapper.findIdOfFansByUserId(userDO.getId());
        List<Integer> idOfFocus = mapper.findIdOfFocusByUserId(userDO.getId());
        userDO.setFocusUsers(StringListConvertUtils.integerListToString(idOfFocus));
        userDO.setIsFocusedUsers(StringListConvertUtils.integerListToString(idOfFans));
    }

    @Override
    public UserDO findUserInfo(int id) {
        UserDO userInfo = mapper.findUserInfo(id);
        if(userInfo != null){
            convertUserProperties(userInfo);
        }
        return userInfo;
    }

    @Override
    public String findPwd(String userName) {
        return mapper.findPwdByName(userName);
    }

    @Override
    public void focusUser(int userId, int focusUserId) {
        //用于判断有没有互相关注
        FocusDO focusDO = mapper.isFocus(focusUserId,userId);
        if(focusDO == null){
            //没有互相关注
            mapper.insertFocus(userId,focusUserId,0);
        }else{
            //互相关注
            mapper.insertFocus(userId,focusUserId,1);
            mapper.updateFocus(focusDO.getId(),1);
        }
    }

    @Override
    public void cancelFocus(int userId, int focusUserId) {
        FocusDO focusDO = mapper.isFocus(focusUserId,userId);
        if(focusDO == null){
            //没有互相关注，只删除user的关注即可
            mapper.deleteFocus(userId,focusUserId);
        }else{
            //互相关注，修改关注用户中，互相关注字段为0，然后删除user的关注
            mapper.updateFocus(focusDO.getId(),0);
            mapper.deleteFocus(userId,focusUserId);
        }
    }

    @Override
    public List<String> findAllCharacters() {
        return mapper.findAllCharacters();
    }

    @Override
    public void setUserCharacters(int userId, List<String> labels) {
        mapper.deleteUserCharacter(userId);
        String hobby = StringListConvertUtils.listToString(labels);
        mapper.updateUserCharacter(userId,hobby);
    }

    @Override
    public List<UserDO> findFans(int userId, int pageNum, int pageSize) {
        PageHelper.startPage(pageNum,pageSize);
        return mapper.findFansByUserId(userId);
    }

    @Override
    public List<UserDO> findFocusUsers(int userId, int pageNum, int pageSize) {
        PageHelper.startPage(pageNum,pageSize);
        return mapper.findFocusUsers(userId);
    }

    @Override
    public List<UserDO> searchUsers(String userName, int pageNum, int pageSize) {
        String appendUserName = "%"+userName+"%";
        PageHelper.startPage(pageNum,pageSize);
        return mapper.searchUser(appendUserName);
    }

    @Override
    public void bindAddress(int userId, String address) {
        mapper.updateAddress(userId,address);
    }

    @Override
    public void authenticateUser(String academy, String major, String studentNum, String name, String cardUrl, Integer id) {
        mapper.authenticateUser(academy,major,studentNum,name,cardUrl,id);
    }

    @Override
    public boolean getAuthenticationState(Integer id) {
        Integer isAuthenticated = mapper.getAuthenticationState(id);
        if(isAuthenticated == null){
            return false;
        } else if(isAuthenticated == 1){
            return true;
        }else{
            return false;
        }
    }

    @Override
    public boolean isApplied(Integer userId, Integer activityId) {
        Integer formByUserAndActivity = mapper.findFormByUserAndActivity(userId, activityId);
        if(formByUserAndActivity != null){
            return true;
        }else{
            return false;
        }
    }
}
