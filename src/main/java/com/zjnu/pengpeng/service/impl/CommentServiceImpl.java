package com.zjnu.pengpeng.service.impl;

import com.github.pagehelper.PageHelper;
import com.zjnu.pengpeng.controller.entity.CommentVO;
import com.zjnu.pengpeng.dao.entity.CommentDO;
import com.zjnu.pengpeng.dao.mapper.CommentMapper;
import com.zjnu.pengpeng.dao.mapper.PostMapper;
import com.zjnu.pengpeng.interceptor.CustomException;
import com.zjnu.pengpeng.service.CommentService;
import com.zjnu.pengpeng.util.WechatUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

/**
 * @author ZhaoBW
 * @version 1.0
 * @date 2020/11/6 20:32
 */
@Service
public class CommentServiceImpl implements CommentService {

    @Autowired
    CommentMapper commentMapper;

    @Autowired
    PostMapper postMapper;

    @Override
    public void commentPost(CommentVO controllerEntity) throws IOException {
        boolean checkContent = WechatUtils.checkContent(controllerEntity.getContent());
        if(checkContent){
            CommentDO daoEntity = CommentDO.convertComment(controllerEntity);
            postMapper.updateCommentCount(controllerEntity.getPost_id());
            commentMapper.insertComment(daoEntity);
        }else{
            throw new CustomException("含有违法违规内容");
        }
    }

    @Override
    public List<CommentDO> findCommentByPostId(int postId, int pageNum, int pageSize) {
        PageHelper.startPage(pageNum,pageSize);
        return commentMapper.findCommentByPost(postId);
    }
}
