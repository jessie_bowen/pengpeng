package com.zjnu.pengpeng.service.impl;

import com.github.pagehelper.PageHelper;
import com.zjnu.pengpeng.controller.entity.TaskVO;
import com.zjnu.pengpeng.dao.entity.TaskDO;
import com.zjnu.pengpeng.dao.mapper.TaskMapper;
import com.zjnu.pengpeng.interceptor.CustomException;
import com.zjnu.pengpeng.service.TaskService;
import com.zjnu.pengpeng.util.WechatUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

/**
 * @author ZhaoBW
 * @version 1.0
 * @date 2020/11/8 16:18
 */
@Service
public class TaskServiceImpl implements TaskService {

    @Autowired
    TaskMapper taskMapper;

    @Override
    public void postTask(TaskVO taskVO) throws IOException {
        boolean checkContent = WechatUtils.checkContent(taskVO.getContent());
        if(checkContent){
            TaskDO taskDO = TaskDO.convertDO(taskVO);
            taskMapper.insertTask(taskDO);
        }else {
            throw new CustomException("含有违法违规内容");
        }
    }

    @Override
    public void endTask(int taskId) {
        taskMapper.endTask(taskId);
    }

    @Override
    public List<TaskDO> findAllTask(int pageNum, int pageSize) {
        PageHelper.startPage(pageNum,pageSize);
        return taskMapper.findAllTask();
    }

    @Override
    public void receiveTask(int task_id, int receiver_id) {
        TaskDO taskDO = taskMapper.findTaskById(task_id);
        if(taskDO == null){
            throw new CustomException("当前任务不存在");
        }
        if(taskDO.getState() >= 1 ){
            throw new CustomException("当前任务不可接收");
        }
        if(taskDO.getUserId() == receiver_id){
            throw new CustomException("不可接收自己发布的任务");
        }
        taskMapper.receiveTask(task_id,receiver_id);
    }

    @Override
    public void finishTask(int task_id) {
        taskMapper.finishTask(task_id);
    }

    @Override
    public void cancelTask(int task_id) {
        taskMapper.cancelTask(task_id);
    }

    @Override
    public List<TaskDO> findTaskByUserId(int userId, int pageNum, int pageSize) {
        PageHelper.startPage(pageNum,pageSize);
        return taskMapper.findTaskByUserId(userId);
    }

    @Override
    public List<TaskDO> findTaskByReceiverId(int receiverId, int pageNum, int pageSize) {
        PageHelper.startPage(pageNum,pageSize);
        return taskMapper.findTaskByReceiverId(receiverId);
    }

    @Override
    public List<TaskDO> findTaskByState(int state, int pageNum, int pageSize) {
        PageHelper.startPage(pageNum,pageSize);
        return taskMapper.findTaskByState(state);
    }
}
