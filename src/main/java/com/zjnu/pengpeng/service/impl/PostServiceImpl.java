package com.zjnu.pengpeng.service.impl;

import com.github.pagehelper.PageHelper;
import com.zjnu.pengpeng.controller.entity.PostVO;
import com.zjnu.pengpeng.dao.entity.PostDO;
import com.zjnu.pengpeng.dao.entity.PostLabelDO;
import com.zjnu.pengpeng.dao.mapper.PostMapper;
import com.zjnu.pengpeng.interceptor.CustomException;
import com.zjnu.pengpeng.service.PostService;
import com.zjnu.pengpeng.util.WechatUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

/**
 * @author ZhaoBW
 * @version 1.0
 * @date 2020/11/6 8:55
 */
@Service
public class PostServiceImpl implements PostService {

    @Autowired
    PostMapper postMapper;

    @Override
    public List<PostDO> findPostByService(int userId, int pageIndex, int pageSize) {
        PageHelper.startPage(pageIndex,pageSize);
        return postMapper.findPostByUser(userId);
    }

    @Override
    public void uploadPost(PostVO controllerEntity) throws IOException {
        boolean checkContent = WechatUtils.checkContent(controllerEntity.getContent());
        if(checkContent){
            PostDO daoEntity = PostDO.convertDaoEntity(controllerEntity);
            postMapper.insertPost(daoEntity);
            postMapper.updateLabel(daoEntity.getLabelId());
        }else {
            throw new CustomException("含有违法违规内容");
        }
    }

    @Override
    public List<PostLabelDO> findAllPostLabels() {
        return postMapper.findAllPostLabels();
    }

    @Override
    public List<PostDO> findPostByLabel(Integer labelId, int pageNum, int pageSize) {
        PageHelper.startPage(pageNum,pageSize);
        return postMapper.findPostByLabel(labelId);
    }

    @Override
    public void updatePraise(int postId) {
        postMapper.updatePraise(postId);
    }

    @Override
    public List<PostDO> findPostByContent(String content, int pageNum, int pageSize) {
        PageHelper.startPage(pageNum,pageSize);
        return postMapper.findPostByContent("%"+content+"%");
    }
}
