package com.zjnu.pengpeng.service;

import com.zjnu.pengpeng.dao.entity.BriefItemDO;
import com.zjnu.pengpeng.dao.entity.CartItemDO;
import com.zjnu.pengpeng.dao.entity.CategoryDO;
import com.zjnu.pengpeng.dao.entity.ItemDO;

import java.util.List;

/**
 * @author ZhaoBW
 * @version 1.0
 * @date 2020/12/7 19:06
 */
public interface ShopService {

    /**
     * 查询所有类别
     * @param shopId 店铺id
     * @return 类别列表
     */
    List<CategoryDO> findCategoryByShopId(int shopId);

    /**
     * 根据商品类别查询商品列表
     * @param categoryId 类别id
     * @param pageNum 页数
     * @param pageSize 容量
     * @return 商品列表
     */
    List<BriefItemDO> findItemByCategory(int categoryId, int pageNum, int pageSize);

    /**
     * 根据id查询商品详情
     * @param id 商品id
     * @return 商品详情
     */
    ItemDO findItemById(int id);

    /**
     * 根据店铺查找所有商品
     * @param shopId 商品id
     * @param pageNum 页码
     * @param pageSize 容量
     * @return 商品列表
     */
    List<ItemDO> findItemByShop(int shopId, int pageNum, int pageSize);
}
