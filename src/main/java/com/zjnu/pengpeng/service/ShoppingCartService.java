package com.zjnu.pengpeng.service;

import com.zjnu.pengpeng.dao.entity.BriefItemDO;
import com.zjnu.pengpeng.dao.entity.CartItemDO;
import com.zjnu.pengpeng.dao.entity.CategoryDO;
import com.zjnu.pengpeng.dao.entity.ItemDO;

import java.util.List;

/**
 * @author ZhaoBW
 * @version 1.0
 * @date 2020/12/7 19:06
 */
public interface ShoppingCartService {

    /**
     * 添加购物车
     * @param userId 用户id
     * @param itemId 商品id
     * @return 购物车item的id
     */
    int addCart(int userId, int itemId);

    /**
     * 增加购物车item的数量
     * @param id itemId
     */
    void increaseCartItemCount(int id);

    /**
     * 减购物车item的数量
     * @param id 购物车itemId
     */
    void decreaseCartItemCount(int id);

    /**
     * 根据用户id查询购物车
     * @param userId 用户id
     * @return 购物车item列表
     */
    List<CartItemDO> findCartItems(int userId);

    /**
     * 删除购物车item
     * @param id itemId
     */
    void deleteCartItem(int id);

}
