package com.zjnu.pengpeng.service;


import com.zjnu.pengpeng.dao.entity.BannerDO;
import java.util.List;

/**
 * @author ZhaoBW
 * @version 1.0
 * @date 2020/11/6 15:00
 */
public interface BannerService {

    /**
     * 查询所有轮播图
     * @return 轮播图列表
     */
    List<BannerDO> findAllBanner();



}
