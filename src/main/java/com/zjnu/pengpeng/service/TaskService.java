package com.zjnu.pengpeng.service;

import com.zjnu.pengpeng.controller.entity.TaskVO;
import com.zjnu.pengpeng.dao.entity.TaskDO;

import java.io.IOException;
import java.util.List;

/**
 * @author ZhaoBW
 * @version 1.0
 * @date 2020/11/8 16:17
 */
public interface TaskService {

    /**
     * 发布任务
     * @param taskVO 任务
     * @throws IOException 内容检测
     */
    void postTask(TaskVO taskVO) throws IOException;

    /**
     * 结束任务
     * @param taskId 任务id
     */
    void endTask(int taskId);

    /**
     * 查询所有任务
     * @param pageNum 页数
     * @param pageSize 容量
     * @return 任务列表
     */
    List<TaskDO> findAllTask(int pageNum, int pageSize);

    /**
     * 接收任务
     * @param task_id 任务id
     * @param receiver_id 接收用户id
     */
    void receiveTask(int task_id, int receiver_id);

    /**
     * 结束任务
     * @param task_id 任务id
     */
    void finishTask(int task_id);

    /**
     * 取消任务
     * @param task_id 任务id
     */
    void cancelTask(int task_id);

    /**
     * 根据userId查询任务
     * @param userId userId
     * @param pageNum pageNum
     * @param pageSize pageSize
     * @return 任务列表
     */
    List<TaskDO> findTaskByUserId(int userId, int pageNum, int pageSize);

    /**
     * 根据receiverId查询任务
     * @param receiverId receiverId
     * @param pageNum pageNum
     * @param pageSize pageSize
     * @return 任务列表
     */
    List<TaskDO> findTaskByReceiverId(int receiverId, int pageNum, int pageSize);

    /**
     * 根据state查询任务
     * @param state 任务状态
     * @param pageNum  pageNum
     * @param pageSize pageSize
     * @return 任务列表
     */
    List<TaskDO> findTaskByState(int state, int pageNum, int pageSize);
}
