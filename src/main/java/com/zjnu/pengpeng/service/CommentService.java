package com.zjnu.pengpeng.service;

import com.zjnu.pengpeng.controller.entity.CommentVO;
import com.zjnu.pengpeng.dao.entity.CommentDO;

import java.io.IOException;
import java.util.List;

/**
 * @author ZhaoBW
 * @version 1.0
 * @date 2020/11/6 20:29
 */
public interface CommentService {

    /**
     * 评论帖子
     * @param controllerEntity
     */
    void commentPost(CommentVO controllerEntity) throws IOException;

    /**
     * 根据帖子id查找评论
     * @param postId 帖子id
     * @param pageNum 页数
     * @param pageSize 容量
     * @return 评论列表
     */
    List<CommentDO> findCommentByPostId(int postId, int pageNum, int pageSize);

}
