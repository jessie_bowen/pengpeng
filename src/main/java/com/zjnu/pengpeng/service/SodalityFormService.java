package com.zjnu.pengpeng.service;

import com.zjnu.pengpeng.controller.entity.SodalityFormVO;
import com.zjnu.pengpeng.dao.entity.ActivityDO;

import java.util.List;

/**
 * @author ZhaoBW
 * @version 1.0
 * @date 2020/11/21 10:02
 */
public interface SodalityFormService {

    /**
     * 插入申请表
     * @param form 表单
     * @return 数据库主键
     */
    int insertForm(SodalityFormVO form);

    /**
     * 查询申请表
     * @param pageNum 页数
     * @param pageSize 容量
     * @return 申请表
     */
    List<SodalityFormVO> findSodalityForm(int pageNum, int pageSize);

    /**
     * 查询目前的活动
     * @return activity
     */
    ActivityDO getActivity();

}
