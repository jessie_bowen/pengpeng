package com.zjnu.pengpeng.service;

import com.zjnu.pengpeng.controller.entity.OrderVO;
import com.zjnu.pengpeng.controller.entity.WechatPayParameterVO;

/**
 * @author ZhaoBW
 * @version 1.0
 * @date 2020/11/19 9:46
 */
public interface OrderService {

    /**
     * 创建订单
     * @param orderVO 接收前端传来的订单类
     * @return 创建的订单id
     * @throws Exception 统一下单失败
     */
    WechatPayParameterVO createOrder(OrderVO orderVO) throws Exception;

}
