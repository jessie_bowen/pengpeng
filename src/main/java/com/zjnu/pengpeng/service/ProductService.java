package com.zjnu.pengpeng.service;

import com.zjnu.pengpeng.controller.entity.ProductVO;
import com.zjnu.pengpeng.dao.entity.ProductDO;

import java.util.List;

/**
 * @author ZhaoBW
 * @version 1.0
 * @date 2020/11/8 21:30
 */
public interface ProductService {

    /**
     * 发布二手商品
     * @param viewProduct 二手商品
     */
    void publishProduct(ProductVO viewProduct);

    /**
     * 查询所有商品
     * @param pageNum 页数
     * @param pageSize 容量
     * @return 商品列表
     */
    List<ProductDO> findAllProduct(int pageNum,int pageSize);

    /**
     * 购买商品
     * @param id 商品id
     * @param buyerId 购买者id
     */
    void purchaseProduct(int id, int buyerId);

    /**
     * 取消购买商品
     * @param id 商品id
     */
    void cancelPurchasingProduct(int id);

    /**
     * 查询卖家商品
     * @param sellerId 卖家id
     * @param pageNum 页数
     * @param pageSize 容量
     * @return 商品列表
     */
    List<ProductDO> findProductBySellId(int sellerId,int pageNum, int pageSize);

    /**
     * 查询买家商品
     * @param buyerId 买家id
     * @param pageNum 页数
     * @param pageSize 容量
     * @return 商品列表
     */
    List<ProductDO> findProductByBuyerId(int buyerId,int pageNum, int pageSize);

    /**
     * 结束商品订单
     * @param id 商品id
     */
    void finishProduct(int id);

}
