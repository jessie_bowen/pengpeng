package com.zjnu.pengpeng.service;


import com.zjnu.pengpeng.dao.entity.UserDO;
import java.util.List;

/**
 * @author zbw
 */
public interface UserService {

    /**
     * 处理登录请求
     * @param code 授权码
     * @param nickName 昵称
     * @param sex 性别
     * @param icon 头像
     * @return 用户信息
     */
    UserDO login(String code, String nickName, String sex, String icon);

    /**
     * 根据用户id获取用户信息
     * @param id 用户id
     * @return 用户信息
     */
    UserDO findUserInfo(int id);


    /**
     * 找回密码
     * @param userName 用户名
     * @return 密码
     */
    String findPwd(String userName);

    /**
     * 关注用户
     * @param userId 用户id
     * @param focusUserId 关注用户id
     */
    void focusUser(int userId, int focusUserId);

    /**
     * 取消关注
     * @param userId 用户id
     * @param focusUserId 关注用户id
     */
    void cancelFocus(int userId, int focusUserId);

    /**
     * 获取所有爱好的标签
     * @return 标签列表
     */
    List<String> findAllCharacters();

    /**
     * 设置用户爱好标签
     * @param userId 用户id
     * @param labels 标签
     */
    void setUserCharacters(int userId,List<String> labels);

    /**
     * 查询用户粉丝
     * @param userId userId
     * @param pageNum 页数
     * @param pageSize 容量
     * @return 用户列表
     */
    List<UserDO> findFans(int userId, int pageNum, int pageSize);

    /**
     * 查询关注的用户
     * @param userId userId
     * @param pageNum 页数
     * @param pageSize 容量
     * @return 用户列表
     */
    List<UserDO> findFocusUsers(int userId, int pageNum, int pageSize);

    /**
     * 搜索用户
     * @param userName 用户名
     * @param pageNum 页数
     * @param pageSize 容量
     * @return 用户列表
     */
    List<UserDO> searchUsers(String userName, int pageNum, int pageSize);

    /**
     * 绑定地址
     * @param userId 用户id
     * @param address 地址
     */
    void bindAddress(int userId, String address);

    /**
     * 修改用户认证状态
     * @param academy 学院
     * @param major 专业
     * @param studentNum 学号
     * @param name 姓名
     * @param cardUrl 学生卡地址
     * @param id user_id
     */
    void authenticateUser(String academy, String major, String studentNum,
                          String name, String cardUrl,Integer id);

    /**
     * 获取用户认证状态
     * @param id 用户id
     * @return 认证状态
     */
    boolean getAuthenticationState(Integer id);

    /**
     * 查询用户是否已经报名
     * @param userId 用户id
     * @param activityId 活动id
     * @return 报名表
     */
    boolean isApplied(Integer userId, Integer activityId);
}
