package com.zjnu.pengpeng.interceptor;

import com.alibaba.fastjson.JSON;
import com.zjnu.pengpeng.controller.entity.JsonData;
import com.zjnu.pengpeng.util.JwtUtils;
import io.jsonwebtoken.Claims;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * @author ZhaoBW
 * @version 1.0
 * @date 2020/11/10 20:57
 */
public class LoginInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String access_token = request.getHeader("token");
        if(!StringUtils.isEmpty(access_token)){
            Claims claims = JwtUtils.checkJWT(access_token);
            if(claims != null){
                return true;
            }
        }
        sendMessage(response);
        return false;
    }

    private static void sendMessage(HttpServletResponse response){
        response.setContentType("application/json; charset = utf-8");

        PrintWriter writer = null;
        try {
            writer = response.getWriter();
            JsonData jsonData = new JsonData(410,"请登录");
            writer.print(JSON.toJSONString(jsonData));
            response.flushBuffer();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            writer.close();
        }
    }

}
