package com.zjnu.pengpeng.interceptor;

/**
 * @author ZhaoBW
 * @version 1.0
 * @date 2020/11/27 9:44
 */
public class CustomException extends RuntimeException {

    public CustomException(String message){
        super(message);
    }

}
