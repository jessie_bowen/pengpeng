package com.zjnu.pengpeng.interceptor;


import com.zjnu.pengpeng.controller.entity.JsonData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import javax.servlet.http.HttpServletRequest;



/**
 * 全局异常捕获
 * @author zbw
 */
@RestControllerAdvice
public class CustomExtHandler {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @ExceptionHandler(value = CustomException.class)
    public JsonData handleCustomException(CustomException e, HttpServletRequest request){
        logger.error("CustomExtHandler: URL: "+request.getRequestURL() + ",exception: " + e.getMessage());
        return new JsonData(-1,e.getMessage());
    }

    @ExceptionHandler(value = Exception.class)
    public JsonData handleException(Exception e, HttpServletRequest request){
        logger.error("CustomExtHandler: URL: "+request.getRequestURL() + ",exception: " + e.getMessage());
        return new JsonData(-1,"服务异常，请稍后重试");
    }

}
