package com.zjnu.pengpeng.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;


/**
 * 微信配置类
 * @author zbw
 */
@Configuration
@PropertySource(value="classpath:/application.yml")
@Data
public class WeChatConfig {


    /**
     * 小程序appid
     */
    @Value("${wxpay.appid}")
    private String appId;

    /**
     * 小程序秘钥
     */
    @Value("${wxpay.app_secret}")
    private String appSecret;

    /**
     * 商户号id
     */
    @Value("${wxpay.mch_id}")
    private String mchId;

    /**
     * 商户号API密钥
     */
    @Value("${wxpay.key}")
    private String key;

    /**
     * 微信支付回调url
     */
    @Value("${wxpay.callback}")
    private String payCallbackUrl;

    /**
     * 统一下单URL
     */
    public static final String UNIFIED_ORDER_URL = "https://api.mch.weixin.qq.com/pay/unifiedorder";

}
