package com.zjnu.pengpeng.config;

import com.zjnu.pengpeng.interceptor.LoginInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * 拦截器配置
 * @author ZhaoBW
 * @version 1.0
 * @date 2020/11/10 21:25
 */
@Configuration
public class InterceptorConfig implements WebMvcConfigurer {
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new LoginInterceptor())
                .addPathPatterns("/api/product/purchase_product")
                .addPathPatterns("/api/product/get_product_by_seller_id")
                .addPathPatterns("/api/product/get_product_by_buyer_id")
                .addPathPatterns("/api/product/publish_product")
                .addPathPatterns("/api/product/cancel_purchasing_product")
                .addPathPatterns("/api/product/finish_product")
                .addPathPatterns("/api/comment/comment_post")
                .addPathPatterns("/api/post/get_user_posts")
                .addPathPatterns("/api/post/upload_post")
                .addPathPatterns("/api/post/praise_post")
//                .addPathPatterns("/api/sodality_form/post_sodality_form")
//                .addPathPatterns("/api/sodality_form/get_sodality_form")
                .addPathPatterns("/api/task/publish_task")
                .addPathPatterns("/api/task/receive_task")
                .addPathPatterns("/api/task/finish_task")
                .addPathPatterns("/api/task/cancel_task")
                .addPathPatterns("/api/task/get_task_by_user_id")
                .addPathPatterns("/api/task/get_task_by_receiver_id")
                .addPathPatterns("/api/user/focus_user")
                .addPathPatterns("/api/user/cancel_focus_user")
                .addPathPatterns("/api/user/set_user_character")
                .addPathPatterns("/api/user/get_fans")
                .addPathPatterns("/api/user/get_focus_users")
                .addPathPatterns("/api/user/bind_address");
//                .addPathPatterns("/api/user/get_user_info");
        WebMvcConfigurer.super.addInterceptors(registry);
    }
}
