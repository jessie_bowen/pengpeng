package com.zjnu.pengpeng.config;

import com.baidubce.auth.DefaultBceCredentials;
import com.baidubce.services.bos.BosClient;
import com.baidubce.services.bos.BosClientConfiguration;
import com.baidubce.services.bos.model.CannedAccessControlList;
import com.baidubce.services.bos.model.PutObjectResponse;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;

/**
 * 百度云BOS上传文件配置类
 * @author ZhaoBW
 * @version 1.0
 * @date 2020/11/8 8:19
 */
@Configuration
public class BosFileConfig {

    private static final String ACCESS_KEY_ID = "a7e89ddcade648538a910eb6a3c153bb";
    private static final String SECRET_ACCESS_KEY = "5c21c3b3ee3748b9868a57918f7b60f7";
    private static final String ENDPOINT = "su.bcebos.com";
    private static final String BUCKET_NAME = "zjnu-pengpeng";

    /**
     * BOS对象存储客户端
     * @return 客户端
     */
    @Bean
    public BosClient bosClient(){
        BosClientConfiguration config = new BosClientConfiguration();
        //配置证书
        config.setCredentials(new DefaultBceCredentials(ACCESS_KEY_ID,SECRET_ACCESS_KEY));
        //配置端点区域，华东苏州区
        config.setEndpoint(ENDPOINT);
        BosClient client = new BosClient(config);
        //配置Bucket名称，及读写控制
        client.setBucketAcl(BUCKET_NAME, CannedAccessControlList.PublicReadWrite);
        return client;
    }

    public String uploadFile(MultipartFile file) throws IOException {
        BosClient client = bosClient();
        //获取文件输入流
        InputStream inputStream = file.getInputStream();
        String originalFilename = file.getOriginalFilename();
        String suffixName = originalFilename.substring(originalFilename.lastIndexOf("."));
        String fileName = UUID.randomUUID().
                toString().replace("-", "").toLowerCase()+suffixName;
        //生成随机字符串作为文件的key，上传文件。
        client.putObject(BUCKET_NAME, fileName, inputStream);
        return fileName;
    }



}
