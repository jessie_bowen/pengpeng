package com.zjnu.pengpeng;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author zbw
 */
@SpringBootApplication
@MapperScan("com.zjnu.pengpeng.dao.mapper")
public class PengpengApplication {

    public static void main(String[] args) {
        SpringApplication.run(PengpengApplication.class, args);
    }

}
