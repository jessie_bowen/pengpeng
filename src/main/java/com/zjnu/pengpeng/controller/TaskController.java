package com.zjnu.pengpeng.controller;

import com.zjnu.pengpeng.controller.entity.JsonData;
import com.zjnu.pengpeng.controller.entity.TaskVO;
import com.zjnu.pengpeng.dao.entity.TaskDO;
import com.zjnu.pengpeng.service.TaskService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;

/**
 * @author ZhaoBW
 * @version 1.0
 * @date 2020/11/8 16:30
 */

@RestController
@RequestMapping("api/task")
@Api(tags = {"任务API"})
@Slf4j
public class TaskController {

    @Autowired
    TaskService taskService;

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @ApiOperation("发布任务")
    @RequestMapping(value = "publish_task", method = RequestMethod.POST)
    public JsonData postTask(@RequestBody TaskVO task) throws IOException {
        taskService.postTask(task);
        logger.info("postTask params: task: " + task
                +",return: success" );
        return new JsonData(200,"success");
    }

    @ApiOperation("查询任务列表")
    @RequestMapping(value = "get_task", method = RequestMethod.POST)
    public JsonData getTask(int page_num, int page_size){
        List<TaskDO> daoList = taskService.findAllTask(page_num, page_size);
        List<TaskVO> controllerList = TaskVO.convertVO(daoList);
        logger.info("getTask params: "
                + "page_num: " + page_num
                + "page_size: " + page_size
                + ",return: " + controllerList);
        return new JsonData(200,controllerList);
    }

    @ApiOperation("接收任务")
    @RequestMapping(value = "receive_task", method = RequestMethod.POST)
    public JsonData receiveTask(int task_id, int receiver_id){
        taskService.receiveTask(task_id,receiver_id);
        logger.info("receiveTask params: "
                + "task_id: " + task_id
                + "receiver_id: " + receiver_id
                + ",return: success");
        return new JsonData(200,"success");
    }

    @ApiOperation("完成任务")
    @RequestMapping(value = "finish_task", method = RequestMethod.POST)
    public JsonData finishTask(int task_id){
        taskService.finishTask(task_id);
        logger.info("finishTask params: "
                + "task_id: " + task_id
                + ",return: success");
        return new JsonData(200,"success");
    }

    @ApiOperation("取消任务")
    @RequestMapping(value = "cancel_task", method = RequestMethod.POST)
    public JsonData cancelTask(int task_id){
        taskService.cancelTask(task_id);
        logger.info("finishTask params: "
                + "task_id: " + task_id
                + ",return: success");
        return new JsonData(200,"success");
    }

    @ApiOperation("结束任务")
    @RequestMapping(value = "end_task", method = RequestMethod.POST)
    public JsonData endTask(int task_id){
        taskService.endTask(task_id);
        logger.info("endTask params: "
                + "task_id: " + task_id
                + ",return: success");
        return new JsonData(200,"success");
    }

    @ApiOperation("查询用户发布的任务")
    @RequestMapping(value = "get_task_by_user_id", method = RequestMethod.POST)
    public JsonData getTaskByUserId(int user_id, int page_num, int page_size){
        List<TaskDO> daoList = taskService.findTaskByUserId(user_id, page_num, page_size);
        List<TaskVO> controllerList = TaskVO.convertVO(daoList);
        logger.info("getTaskByUserId params: "
                + "user_id: " + user_id
                + "page_num: " + page_num
                + "page_size: " + page_size
                + ",return: " + controllerList);
        return new JsonData(200,controllerList);
    }

    @ApiOperation("查询用户接收的任务")
    @RequestMapping(value = "get_task_by_receiver_id", method = RequestMethod.POST)
    public JsonData getTaskByReceiverId(int receiver_id, int page_num, int page_size){
        List<TaskDO> daoList = taskService.findTaskByReceiverId(receiver_id,page_num, page_size);
        List<TaskVO> controllerList = TaskVO.convertVO(daoList);
        logger.info("getTask params: "
                + "getTaskByReceiverId: " + receiver_id
                + "page_num: " + page_num
                + "page_size: " + page_size
                + ",return: " + controllerList);
        return new JsonData(200,controllerList);
    }

    @ApiOperation("根据状态查询任务")
    @RequestMapping(value = "get_task_by_state", method = RequestMethod.POST)
    public JsonData getTaskByState(int state, int page_num, int page_size){
        List<TaskDO> daoList = taskService.findTaskByState(state,page_num, page_size);
        List<TaskVO> controllerList = TaskVO.convertVO(daoList);
        logger.info("getTaskByState params: "
                + "state: " + state
                + "page_num: " + page_num
                + "page_size: " + page_size
                + ",return: " + controllerList);
        return new JsonData(200,controllerList);
    }

}
