package com.zjnu.pengpeng.controller;

import com.zjnu.pengpeng.controller.entity.JsonData;
import com.zjnu.pengpeng.controller.entity.PostVO;
import com.zjnu.pengpeng.dao.entity.PostDO;
import com.zjnu.pengpeng.dao.entity.PostLabelDO;
import com.zjnu.pengpeng.service.PostService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;

/**
 * @author ZhaoBW
 * @version 1.0
 * @date 2020/11/6 8:15
 */
@RestController
@RequestMapping("api/post")
@Api(tags = {"帖子API"})
@Slf4j
public class PostController {

    @Autowired
    PostService postService;

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @ApiOperation("获取用户已发帖子")
    @RequestMapping(value = "get_user_posts", method = RequestMethod.GET)
    public JsonData getUserPosts(int user_id,int page_num,int page_size){
        List<PostDO> postDaoEntities = postService.findPostByService(user_id,page_num,page_size);
        List<PostVO> controllerEntities = PostVO.convertControllerList(postDaoEntities);
        logger.info("getUserPosts params: user_id: " + user_id
                + ",page_index: " + page_num
                + ",page_size: " + page_size
                +",return: " + controllerEntities);
        return new JsonData(200,controllerEntities);
    }

    @ApiOperation("发布帖子")
    @RequestMapping(value = "upload_post", method = RequestMethod.POST)
    public JsonData uploadPost(@RequestBody PostVO entity) throws IOException {
        postService.uploadPost(entity);
        logger.info("uploadPost: params:"+entity
                +",return: success");
        return new JsonData(200,"success");
    }

    @ApiOperation("查询所有帖子标签")
    @RequestMapping(value = "get_post_labels", method = RequestMethod.GET)
    public JsonData getAllPostLabels(){
        List<PostLabelDO> allPostLabels = postService.findAllPostLabels();
        logger.info("getAllPostLabels: "
                +",return: " + allPostLabels);
        return new JsonData(200,allPostLabels);
    }

    @ApiOperation("根据标签查询帖子")
    @RequestMapping(value = "get_post_by_label", method = RequestMethod.GET)
    public JsonData getPostByLabel(Integer label_id,int page_num,int page_size){
        List<PostDO> postDaoEntities = postService.findPostByLabel(label_id,page_num,page_size);
        List<PostVO> controllerEntities = PostVO.convertControllerList(postDaoEntities);
        logger.info("getPostByLabel params: label_id: " + label_id
                + ",page_index: " + page_num
                + ",page_size: " + page_size
                +",return: " + controllerEntities);
        return new JsonData(200,controllerEntities);
    }

    @ApiOperation("点赞帖子")
    @RequestMapping(value = "praise_post", method = RequestMethod.GET)
    public JsonData praisePost(int post_id){
        postService.updatePraise(post_id);
       logger.info("praisePost params: post_id: " + post_id
                +",return: success");
        return new JsonData(200,"success");
    }

    @ApiOperation("搜索帖子")
    @RequestMapping(value = "search_post", method = RequestMethod.GET)
    public JsonData searchPost(String content, int page_num, int page_size){
        List<PostDO> postDaoEntities = postService.findPostByContent(content,page_num,page_size);
        List<PostVO> controllerEntities = PostVO.convertControllerList(postDaoEntities);
        logger.info("getPostByLabel params: content: " + content
                + ",page_index: " + page_num
                + ",page_size: " + page_size
                +",return: " + controllerEntities);
        return new JsonData(200,controllerEntities);
    }
}
