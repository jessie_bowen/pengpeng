package com.zjnu.pengpeng.controller;

import com.zjnu.pengpeng.controller.entity.JsonData;
import com.zjnu.pengpeng.controller.entity.ProductVO;
import com.zjnu.pengpeng.dao.entity.ProductDO;
import com.zjnu.pengpeng.service.ProductService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author ZhaoBW
 * @version 1.0
 * @date 2020/11/8 21:26
 */
@RestController
@RequestMapping("api/product")
@Api(tags = {"二手商品API"})
@Slf4j
public class ProductController {

    @Autowired
    ProductService productService;

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @ApiOperation("发布二手商品")
    @RequestMapping(value = "publish_product", method = RequestMethod.POST)
    public JsonData publishProduct(ProductVO product){
        productService.publishProduct(product);
        logger.info("publishProduct params: product" + product
                + ",return: success");
        return new JsonData(200,"success");
    }

    @ApiOperation("查询所有二手商品")
    @RequestMapping(value = "get_all_product", method = RequestMethod.GET)
    public JsonData getAllProduct(int page_num, int page_size){
        List<ProductDO> doList = productService.findAllProduct(page_num,page_size);
        List<ProductVO> controllerList = ProductVO.convertVOList(doList);
        logger.info("getAllProduct params: page_num: " + page_num
                + ",page_size: " + page_size
                +",return: " + controllerList);
        return new JsonData(200,controllerList);
    }

    @ApiOperation("购买商品")
    @RequestMapping(value = "purchase_product", method = RequestMethod.GET)
    public JsonData purchaseProduct(int product_id, int buyer_id){
        productService.purchaseProduct(product_id,buyer_id);
        logger.info("purchaseProduct params: product_id: " + product_id
                + ",buyer_id: " + buyer_id
                +",return: success");
        return new JsonData(200,"success");
    }

    @ApiOperation("取消购买")
    @RequestMapping(value = "cancel_purchasing_product", method = RequestMethod.GET)
    public JsonData cancelPurchasingProduct(int product_id){
        productService.cancelPurchasingProduct(product_id);
        logger.info("cancelPurchasingProduct params: product_id: " + product_id
                +",return: success");
        return new JsonData(200,"success");
    }

    @ApiOperation("查询卖家商品")
    @RequestMapping(value = "get_product_by_seller_id", method = RequestMethod.GET)
    public JsonData getProductBySellerId(int seller_id, int page_num, int page_size){
        List<ProductDO> doList = productService.findProductBySellId(seller_id,page_num,page_size);
        List<ProductVO> controllerList = ProductVO.convertVOList(doList);
        logger.info("getProductBySellerId params: seller_id: " + seller_id
                + ",page_num: " + page_num
                + ",page_size: " + page_size
                +",return: " + controllerList);
        return new JsonData(200,controllerList);
    }

    @ApiOperation("查询买家商品")
    @RequestMapping(value = "get_product_by_buyer_id", method = RequestMethod.GET)
    public JsonData getProductByBuyerId(int buyer_id,int page_num, int page_size){
        List<ProductDO> doList = productService.findProductByBuyerId(buyer_id,page_num,page_size);
        List<ProductVO> controllerList = ProductVO.convertVOList(doList);
        logger.info("getProductByBuyerId params: buyer_id: " + buyer_id
                + ",page_num: " + page_num
                + ",page_size: " + page_size
                +",return: " + controllerList);
        return new JsonData(200,controllerList);
    }

    @ApiOperation("结束商品")
    @RequestMapping(value = "finish_product", method = RequestMethod.GET)
    public JsonData finishProduct(int product_id){
        productService.finishProduct(product_id);
        logger.info("finishProduct params: product_id: " + product_id
                +",return: success");
        return new JsonData(200,"success");
    }

}
