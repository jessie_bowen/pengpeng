package com.zjnu.pengpeng.controller;

import com.zjnu.pengpeng.controller.entity.JsonData;
import com.zjnu.pengpeng.controller.entity.OrderVO;
import com.zjnu.pengpeng.controller.entity.WechatPayParameterVO;
import com.zjnu.pengpeng.service.OrderService;
import com.zjnu.pengpeng.util.CommonUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * @author ZhaoBW
 * @version 1.0
 * @date 2020/11/19 10:10
 */
@Api(tags = {"订单接口"})
@RestController
@RequestMapping("api/order")
public class OrderController {

    @Autowired
    OrderService orderService;

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @ApiOperation("创建订单")
    @RequestMapping(value = "create_order", method = RequestMethod.POST)
    public JsonData createOrder(OrderVO orderVO, HttpServletRequest request) throws Exception {
        String ip = CommonUtils.getIpAddr(request);
        orderVO.setIp(ip);
        WechatPayParameterVO orderServiceOrder = orderService.createOrder(orderVO);
        logger.info("createOrder: param:" + orderVO +
                ",return: " + orderServiceOrder);
        return new JsonData(200,orderServiceOrder);
    }


}
