package com.zjnu.pengpeng.controller;

import com.zjnu.pengpeng.controller.entity.JsonData;
import com.zjnu.pengpeng.interceptor.CustomException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author ZhaoBW
 * @version 1.0
 * @date 2020/11/27 9:51
 */
@Api(tags = {"异常测试接口"})
@RequestMapping("api/test")
@RestController
public class ExceptionTestController {

    @ApiOperation(value = "异常测试")
    @RequestMapping(value = "exception",method = RequestMethod.GET)
    public JsonData testException(int param){
        if(param == 1){
            throw new CustomException("自定义异常消息");
        }else{
            System.out.println(1/0);
        }
        return new JsonData(200,"success");
    }

}
