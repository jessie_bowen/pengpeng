package com.zjnu.pengpeng.controller;


import com.zjnu.pengpeng.controller.entity.JsonData;
import com.zjnu.pengpeng.dao.entity.CartItemDO;
import com.zjnu.pengpeng.service.ShoppingCartService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author ZhaoBW
 * @version 1.0
 * @date 2020/11/6 14:52
 */
@Api(tags = {"购物车API"})
@RestController
@RequestMapping("api/shopping_cart")
public class ShoppingCartController {

    @Autowired
    ShoppingCartService cartService;

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @ApiOperation("添加购物车")
    @RequestMapping(value = "add_shopping_cart",method = RequestMethod.POST)
    public JsonData addCart(int userId, int itemId){
        int cartId = cartService.addCart(userId, itemId);
        logger.info("addCart: " +
                "param: userId:" + userId +
                ", itemId:" + itemId +
                ", return: " + cartId);
        return new JsonData(200,cartId);
    }

    @ApiOperation("增加购物车item的数量(对应“+”或者“-”的添加操作)")
    @RequestMapping(value = "increase_cart_item_count",method = RequestMethod.POST)
    public JsonData increaseCartItemCount(int itemId){
        cartService.increaseCartItemCount(itemId);
        logger.info("addCartItemCount: " +
                "param: itemId:" + itemId +
                ", return: " + "success");
        return new JsonData(200,"success");
    }

    @ApiOperation("减少购物车item的数量(对应“+”或者“-”的减少操作)")
    @RequestMapping(value = "decrease_cart_item_count",method = RequestMethod.POST)
    public JsonData decreaseCartItemCount(int itemId){
        cartService.decreaseCartItemCount(itemId);
        logger.info("decreaseCartItemCount: " +
                "param: itemId:" + itemId +
                ", return: " + "success");
        return new JsonData(200,"success");
    }

    @ApiOperation("查询购物车")
    @RequestMapping(value = "find_cart_item",method = RequestMethod.GET)
    public JsonData findCartItem(int userId){
        List<CartItemDO> cartItems = cartService.findCartItems(userId);
        logger.info("findCartItem: " +
                "param: userId:" + userId +
                ", return: " + cartItems);
        return new JsonData(200,cartItems);
    }

    @ApiOperation("删除购物车item")
    @RequestMapping(value = "delete_cart_item",method = RequestMethod.GET)
    public JsonData deleteCartItem(int id){
        cartService.deleteCartItem(id);
        logger.info("deleteCartItem: " +
                "param: id:" + id +
                ", return: " + "success");
        return new JsonData(200,"success");
    }

}
