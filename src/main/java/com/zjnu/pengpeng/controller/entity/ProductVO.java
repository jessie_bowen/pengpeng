package com.zjnu.pengpeng.controller.entity;


import com.zjnu.pengpeng.dao.entity.ProductDO;
import lombok.Data;
import org.springframework.util.StringUtils;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author ZhaoBW
 * @version 1.0
 * @date 2020/11/8 20:42
 */
@Data
public class ProductVO {

    private Integer id;
    private String title;
    private String content;
    private Double price;
    private Double original_price;
    private String address;
    private String phone;
    private String classify;
    private String fresh_degree;
    private String trade_type;
    private String time;
    private Integer seller_id;
    private String seller_nick_name;
    private String seller_icon;
    private Boolean is_purchased;
    private Boolean is_finished;
    private Integer view_count;
    private Integer buyer_id;
    private String buyer_nick_name;
    private String buyer_icon;
    private String buyer_address;
    private List<String> imgs;

    private static ProductVO convertVO(ProductDO productDO){
        ProductVO vo = new ProductVO();
        vo.setId(productDO.getId());
        vo.setTitle(productDO.getTitle());
        vo.setContent(productDO.getContent());
        vo.setPrice(productDO.getPrice());
        vo.setOriginal_price(productDO.getOriginalPrice());
        vo.setAddress(productDO.getAddress());
        vo.setPhone(productDO.getPhone());
        vo.setClassify(productDO.getClassify());
        vo.setFresh_degree(productDO.getFreshDegree());
        vo.setTrade_type(productDO.getTradeType());
        vo.setTime(productDO.getTime());
        vo.setSeller_id(productDO.getSellerId());
        vo.setSeller_nick_name(productDO.getSellerNickName());
        vo.setSeller_icon(productDO.getSellerIcon());
        if(productDO.getIsPurchased() == 0){
            vo.setIs_purchased(false);
        }else{
            vo.setIs_purchased(true);
        }

        if(productDO.getIsFinished() == 0){
            vo.setIs_finished(false);
        }else{
            vo.setIs_finished(true);
        }

        vo.setView_count(productDO.getViewCount());
        vo.setBuyer_id(productDO.getBuyerId());
        vo.setBuyer_nick_name(productDO.getBuyerNickName());
        vo.setBuyer_icon(productDO.getBuyerIcon());
        vo.setBuyer_address(productDO.getBuyerAddress());
        String images = productDO.getImage();
        if(!StringUtils.isEmpty(images)){
            String[] imageArray = images.split(",");
            vo.setImgs(Arrays.asList(imageArray));
        }

        return vo;
    }

    public static List<ProductVO> convertVOList(List<ProductDO> doList){
        List<ProductVO> voList = new ArrayList<>(doList.size());
        for (ProductDO productDO : doList) {
            ProductVO vo = convertVO(productDO);
            voList.add(vo);
        }
        return voList;
    }

}
