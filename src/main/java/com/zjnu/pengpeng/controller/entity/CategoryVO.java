package com.zjnu.pengpeng.controller.entity;

import com.zjnu.pengpeng.dao.entity.CategoryDO;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ZhaoBW
 * @version 1.0
 * @date 2020/12/7 18:54
 */

@Data
public class CategoryVO {


    private Integer id;
    private Integer shopId;
    private String name;

    public static CategoryVO convertVO(CategoryDO categoryDO){
        CategoryVO categoryVO = new CategoryVO();
        categoryVO.setId(categoryDO.getId());
        categoryVO.setName(categoryDO.getName());
        categoryVO.setShopId(categoryDO.getShopId());
        return categoryVO;
    }

    public static List<CategoryVO> convertList(List<CategoryDO> categoryDOList){
        List<CategoryVO> categoryVOList = new ArrayList<>(categoryDOList.size());
        for (CategoryDO categoryDO : categoryDOList) {
            CategoryVO categoryVO = convertVO(categoryDO);
            categoryVOList.add(categoryVO);
        }
        return categoryVOList;
    }


}
