package com.zjnu.pengpeng.controller.entity;


import com.zjnu.pengpeng.dao.entity.UserDO;
import com.zjnu.pengpeng.util.JwtUtils;
import lombok.Data;
import java.util.Arrays;
import java.util.List;

/**
 * @author zbw
 */
@Data
public class UserVO {
    private Integer id;
    private String user_name;
    private String hobby;
    private String icon;
    private String phone;
    private String sex;
    private String address;
    private List<String> focused_users;
    private List<String> is_focused_users;
    private String access_token;
    private String nick_name;

    /**
     * 将数据层返回的userDao对象,转换成返回给前端的user
     * @param userDO 数据层返回的userDao对象
     * @return 返回给前端的user
     */
    public static UserVO convertUser(UserDO userDO, boolean isGenerateToken){
        UserVO user = new UserVO();
        user.setId(userDO.getId());
        user.setUser_name(userDO.getUserName());
        user.setHobby(userDO.getHobby());
        user.setIcon(userDO.getIcon());
        user.setPhone(userDO.getPhone());
        user.setSex(userDO.getSex());
        user.setAddress(userDO.getAddress());
        user.setNick_name(userDO.getNickName());

        String strFocusUsers = userDO.getFocusUsers();
        if(strFocusUsers != null && !"".equals(strFocusUsers)){
            String[] arrayFocusUsers = userDO.getFocusUsers().split(",");
            user.setFocused_users(Arrays.asList(arrayFocusUsers));
        }

        String strIsFocusedUsers = userDO.getIsFocusedUsers();
        if(strIsFocusedUsers != null && !"".equals(strIsFocusedUsers)){
            String[] arrayIsFocusedUsers = userDO.getIsFocusedUsers().split(",");
            user.setIs_focused_users(Arrays.asList(arrayIsFocusedUsers));
        }

        //添加access_token属性
        if(isGenerateToken){
            user.setAccess_token(JwtUtils.generateWebToken(user));
        }

        return user;
    }


}
