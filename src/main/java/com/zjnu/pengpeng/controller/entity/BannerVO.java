package com.zjnu.pengpeng.controller.entity;

import com.zjnu.pengpeng.dao.entity.BannerDO;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ZhaoBW
 * @version 1.0
 * @date 2020/11/6 15:04
 */
@Data
public class BannerVO {
    private Integer id;
    private String url;

    public static List<BannerVO> convertBanner(List<BannerDO> daoEntityList){
        List<BannerVO> result = new ArrayList<>(daoEntityList.size());
        for (BannerDO daoEntity : daoEntityList) {
            BannerVO entity = new BannerVO();
            entity.setId(daoEntity.getBusinessId());
            entity.setUrl(daoEntity.getUrl());
            result.add(entity);
        }
        return result;
    }

}
