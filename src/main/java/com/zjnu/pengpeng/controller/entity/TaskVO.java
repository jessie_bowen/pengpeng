package com.zjnu.pengpeng.controller.entity;

import com.zjnu.pengpeng.dao.entity.TaskDO;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ZhaoBW
 * @version 1.0
 * @date 2020/11/8 15:24
 */
@Data
public class TaskVO {

    private Integer id;
    private Integer user_id;
    private String nick_name;
    private String user_icon;
    private String user_sex;
    private String address;
    private String content;
    private Integer state;
    private String phone;
    private Integer receiver_id;
    private String receiver_nick_name;
    private String receiver_icon;
    private String receiver_phone;
    private String receiver_address;
    private String receiver_sex;
    private Double reward;
    private String time;
    private String category;

    public static List<TaskVO> convertVO(List<TaskDO> daoList){
        List<TaskVO> controllerList = new ArrayList<>(daoList.size());
        for (TaskDO daoTask : daoList) {
            TaskVO controllerTask = new TaskVO();
            controllerTask.setId(daoTask.getId());
            controllerTask.setUser_id(daoTask.getUserId());
            controllerTask.setNick_name(daoTask.getNickName());
            controllerTask.setUser_icon(daoTask.getUserIcon());
            controllerTask.setUser_sex(daoTask.getUserSex());
            controllerTask.setAddress(daoTask.getAddress());
            controllerTask.setContent(daoTask.getContent());
            controllerTask.setPhone(daoTask.getPhone());
            controllerTask.setReceiver_id(daoTask.getReceiverId());
            controllerTask.setReceiver_nick_name(daoTask.getReceiverNickName());
            controllerTask.setReceiver_icon(daoTask.getReceiverIcon());
            controllerTask.setReceiver_phone(daoTask.getReceiverPhone());
            controllerTask.setReceiver_address(daoTask.getReceiverAddress());
            controllerTask.setReceiver_sex(daoTask.getReceiverSex());
            controllerTask.setReward(daoTask.getReward());
            controllerTask.setTime(daoTask.getTime());
            controllerTask.setCategory(daoTask.getCategory());
            controllerTask.setState(daoTask.getState());
            controllerList.add(controllerTask);
        }

        return controllerList;
    }


}
