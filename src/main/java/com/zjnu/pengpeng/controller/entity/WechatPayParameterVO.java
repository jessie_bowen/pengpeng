package com.zjnu.pengpeng.controller.entity;

import lombok.Data;

/**
 * @author ZhaoBW
 * @version 1.0
 * @date 2020/11/20 15:00
 */
@Data
public class WechatPayParameterVO {

    private String timeStamp;
    private String nonceStr;
    private String packages;
    private String signType;
    private String paySign;

}
