package com.zjnu.pengpeng.controller.entity;

import com.zjnu.pengpeng.dao.entity.BriefUserDO;
import com.zjnu.pengpeng.dao.entity.UserDO;
import lombok.Data;
import java.util.ArrayList;
import java.util.List;

/**
 * @author ZhaoBW
 * @version 1.0
 * @date 2020/11/6 10:26
 */
@Data
public class BriefUserVO {

    private Integer user_id;
    private String nick_name;
    private String icon;
    private String sex;
    private String hobby;

    public static List<BriefUserVO> convertBriefUser(List<UserDO> userDaoEntities){
        List<BriefUserVO> result = new ArrayList<>(userDaoEntities.size());
        for(UserDO entity: userDaoEntities){
            BriefUserVO fan = new BriefUserVO();
            fan.setUser_id(entity.getId());
            fan.setNick_name(entity.getNickName());
            fan.setIcon(entity.getIcon());
            fan.setSex(entity.getSex());
            fan.setHobby(entity.getHobby());
            result.add(fan);
        }
        return result;
    }
    
    public static BriefUserVO convertBriefUser(BriefUserDO userDO){
        BriefUserVO userVO = new BriefUserVO();
        userVO.setUser_id(userDO.getUserId());
        userVO.setNick_name(userDO.getNickName());
        userVO.setIcon(userDO.getUserIcon());
        userVO.setSex(userDO.getSex());
        return userVO;
    }

}
