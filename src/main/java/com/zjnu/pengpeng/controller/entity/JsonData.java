package com.zjnu.pengpeng.controller.entity;

import java.io.Serializable;

/**
 * @author zbw
 */
public class JsonData implements Serializable {

    private int status;

    private Object msg;

    public JsonData(int status,Object msg){
        this.status = status;
        this.msg = msg;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Object getMsg() {
        return msg;
    }

    public void setMsg(Object msg) {
        this.msg = msg;
    }
}
