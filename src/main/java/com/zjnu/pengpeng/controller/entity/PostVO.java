package com.zjnu.pengpeng.controller.entity;

import com.zjnu.pengpeng.dao.entity.PostDO;
import com.zjnu.pengpeng.util.StringListConvertUtils;
import lombok.Data;
import java.util.ArrayList;
import java.util.List;

/**
 * @author ZhaoBW
 * @version 1.0
 * @date 2020/11/6 9:03
 */
@Data
public class PostVO {

    private Integer id;
    private String nick_name;
    private Integer user_id;
    private String icon;
    private String content;
    private Integer label_id;
    private String time;
    private List<String> imgs;
    private Integer praise_count;
    private Integer comment_count;

    private static PostVO convertControllerEntity(PostDO daoEntity){
        PostVO entity = new PostVO();
        entity.setId(daoEntity.getId());
        entity.setUser_id(daoEntity.getUserId());
        entity.setNick_name(daoEntity.getNickName());
        entity.setIcon(daoEntity.getIcon());
        entity.setContent(daoEntity.getContent());
        entity.setLabel_id(daoEntity.getLabelId());
        entity.setTime(daoEntity.getTime());
        entity.setPraise_count(daoEntity.getPraiseCount());
        entity.setComment_count(daoEntity.getCommentCount());
        entity.setImgs(StringListConvertUtils.stringToList(daoEntity.getImages()));
        return entity;
    }

    public static List<PostVO> convertControllerList(List<PostDO> entities){

        List<PostVO> controllerEntities = new ArrayList<>(entities.size());
        for (PostDO entity : entities) {
            controllerEntities.add(convertControllerEntity(entity));
        }

        return controllerEntities;
    }

}
