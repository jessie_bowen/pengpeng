package com.zjnu.pengpeng.controller.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;


/**
 * @author ZhaoBW
 * @version 1.0
 * @date 2020/11/19 9:29
 */
@Data
@TableName("order_detail")
public class OrderDetailVO {
    @TableId(type = IdType.AUTO)
    private Integer id;
    private Integer order_id;
    private Integer item_id;
    private String item_name;
    private String item_image;
    private String item_price;
    /**
     * 规格字符串
     */
    private String specification;
    private Integer count;

}
