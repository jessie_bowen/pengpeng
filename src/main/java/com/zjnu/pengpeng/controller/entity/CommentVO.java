package com.zjnu.pengpeng.controller.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.zjnu.pengpeng.dao.entity.CommentDO;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ZhaoBW
 * @version 1.0
 * @date 2020/11/6 20:06
 */
@Data
@TableName("comment")
public class CommentVO {

    @TableId(type = IdType.AUTO)
    private Integer id;
    private Integer post_id;
    private String content;
    private String time;
    private Integer user_id;
    private String nick_name;
    private String icon;
    private Integer reply_user_id;
    private String reply_user_nick_name;
    private String reply_user_icon;

    public static List<CommentVO> convertComment
            (List<CommentDO> daoEntityList){
        List<CommentVO> controllerEntityList = new ArrayList<>(daoEntityList.size());
        for (CommentDO daoEntity : daoEntityList) {
            CommentVO entity = new CommentVO();
            entity.setId(daoEntity.getId());
            entity.setPost_id(daoEntity.getPostId());
            entity.setContent(daoEntity.getContent());
            entity.setTime(daoEntity.getTime());
            entity.setUser_id(daoEntity.getUserId());
            entity.setNick_name(daoEntity.getNickName());
            entity.setIcon(daoEntity.getIcon());
            entity.setReply_user_id(daoEntity.getReplyUserId());
            entity.setReply_user_nick_name(daoEntity.getReplyUserNickName());
            entity.setReply_user_icon(daoEntity.getReplyUserIcon());
            controllerEntityList.add(entity);
        }
        return controllerEntityList;
    }


}
