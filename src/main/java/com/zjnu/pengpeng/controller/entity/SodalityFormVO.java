package com.zjnu.pengpeng.controller.entity;

import com.zjnu.pengpeng.dao.entity.SodalityFormDO;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ZhaoBW
 * @version 1.0
 * @date 2020/11/21 9:38
 */
@Data
public class SodalityFormVO {

    private Integer id;
    private Integer userId;
    private String academy;
    private String classes;
    private String name;
    private String student_num;
    private String phone;
    private String wechat_id;
    private String sex;
    private Integer activity_id;

    private static SodalityFormVO convertVO(SodalityFormDO sodalityFormDO){
        SodalityFormVO formVO = new SodalityFormVO();
        formVO.setId(sodalityFormDO.getId());
        formVO.setAcademy(sodalityFormDO.getAcademy());
        formVO.setClasses(sodalityFormDO.getClasses());
        formVO.setName(sodalityFormDO.getName());
        formVO.setStudent_num(sodalityFormDO.getStudentNum());
        formVO.setPhone(sodalityFormDO.getPhone());
        formVO.setWechat_id(sodalityFormDO.getWechatId());
        formVO.setSex(sodalityFormDO.getSex());
        formVO.setUserId(sodalityFormDO.getUserId());
        formVO.setActivity_id(sodalityFormDO.getActivityId());
        return formVO;
    }

    public static List<SodalityFormVO> convertVOList(List<SodalityFormDO> sodalityFormDOList){
        List<SodalityFormVO> voList = new ArrayList<>(sodalityFormDOList.size());
        for (SodalityFormDO formDO : sodalityFormDOList) {
            SodalityFormVO formVO = convertVO(formDO);
            voList.add(formVO);
        }
        return voList;
    }
}
