package com.zjnu.pengpeng.controller.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.List;

/**
 * @author ZhaoBW
 * @version 1.0
 * @date 2020/11/18 19:16
 */
@Data
@TableName("order")
public class OrderVO {

    @TableId(type = IdType.AUTO)
    private Integer id;
    private Integer state;
    /**
     * 订单总额：以分为单位
     * （与微信平台统一字段类型）
     */
    private Integer total_fee;
    private Integer user_id;
    private String nick_name;
    private String icon;
    private Integer address_id;
    private String create_time;
    private List<OrderDetailVO> order_detail_list;
    private String ip;


}
