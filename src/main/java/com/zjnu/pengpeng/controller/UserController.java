package com.zjnu.pengpeng.controller;

import com.zjnu.pengpeng.controller.entity.BriefUserVO;
import com.zjnu.pengpeng.controller.entity.JsonData;
import com.zjnu.pengpeng.controller.entity.Password;
import com.zjnu.pengpeng.controller.entity.UserVO;
import com.zjnu.pengpeng.dao.entity.UserDO;
import com.zjnu.pengpeng.service.UserService;
import com.zjnu.pengpeng.util.JwtUtils;
import io.jsonwebtoken.Claims;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author zbw
 */
@Api(tags = {"用户操作"})
@RestController
@RequestMapping("/api/user/")
@Slf4j
public class UserController {

    @Autowired
    UserService userService;

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @ApiOperation(value = "登录")
    @RequestMapping(value = "login", method = RequestMethod.POST)
    public JsonData login(String code, String nickName, String sex, String icon){
        UserDO userDO = userService.login(code,nickName,sex,icon);
        UserVO userVO = UserVO.convertUser(userDO,true);
        logger.info("login params: " +
                " code:" + code +
                ", nickName:" + nickName +
                ", sex:" + sex +
                ", icon:" + icon + "; return: "+ userVO.toString());
        return new JsonData(200, userVO);
    }

    @ApiOperation(value = "获取用户信息")
    @RequestMapping(value = "get_user_info", method = RequestMethod.POST)
    public JsonData getUserInfo(HttpServletRequest request,String token){
        if(!StringUtils.isEmpty(token)){
            Claims claims = JwtUtils.checkJWT(token);
            if(claims != null){
                UserVO userVO = new UserVO();
                userVO.setId((Integer) claims.get("id"));
                userVO.setNick_name((String) claims.get("nickName"));
                userVO.setSex((String) claims.get("sex"));
                userVO.setIcon((String) claims.get("icon"));
                logger.info("getUserInfo params: token: "+token+"; return:"+ userVO.toString());
                return new JsonData(200,userVO);
            }
        }

//        String access_token = request.getHeader("access_token");
//        if(!StringUtils.isEmpty(access_token)){
//            Claims claims = JwtUtils.checkJWT(access_token);
//            if(claims != null){
//                UserVO userVO = new UserVO();
//                userVO.setId((Integer) claims.get("id"));
//                userVO.setNick_name((String) claims.get("nickName"));
//                userVO.setSex((String) claims.get("sex"));
//                userVO.setIcon((String) claims.get("icon"));
//                logger.info("getUserInfo params: token: "+access_token+"; return:"+ userVO.toString());
//                return new JsonData(200,userVO);
//            }
//        }
        logger.info("getUserInfo params: token: "+token+"; return: 410");
        return new JsonData(410,"请重新登录");

//        UserDO userDO = userService.findUserInfo(user_id);
//        if(userDO == null){
//            logger.info("getUserInfo params: user_id:"+user_id+";return null");
//            return new JsonData(200,null);
//        }else{
//            UserVO userVO = UserVO.convertUser(userDO,false);
//            logger.info("getUserInfo params: user_id:"+user_id+";return:"+ userVO.toString());
//            return new JsonData(200, userVO);
//        }
    }

    @ApiOperation(value = "找回密码")
    @RequestMapping(value = "find_pwd", method = RequestMethod.GET)
    public JsonData findPwd(String user_name){
        String password = userService.findPwd(user_name);
        Password pwd = new Password();
        pwd.setPassword(password);
        logger.info("findPwd params: " + user_name + ",return: "+password);
        return new JsonData(200,pwd);
    }

    @ApiOperation(value = "关注用户")
    @RequestMapping(value = "focus_user", method = RequestMethod.GET)
    public JsonData focusUser(int user_id, int focus_user_id){
        userService.focusUser(user_id,focus_user_id);
        logger.info("focusUser params: user_id" + user_id
                + ",focus_user_id: " + focus_user_id
                +",return: success");
        return new JsonData(200,"success");
    }

    @ApiOperation(value = "取消关注")
    @RequestMapping(value = "cancel_focus_user", method = RequestMethod.POST)
    public JsonData cancelFocusUser(int user_id, int focus_user_id){
        userService.cancelFocus(user_id,focus_user_id);
        logger.info("cancelFocusUser params: user_id" + user_id
                + ",focus_user_id: " + focus_user_id
                +",return: success");
        return new JsonData(200,"success");
    }

    @ApiOperation(value = "获取爱好标签")
    @RequestMapping(value = "get_characters", method = RequestMethod.GET)
    public JsonData getCharacters(){
        List<String> labels = userService.findAllCharacters();
        StringBuilder sb = new StringBuilder();
        labels.forEach(sb::append);
        logger.info("get_characters return:"+sb.toString());
        return new JsonData(200,labels);
    }

    @ApiOperation(value = "设置用户爱好标签")
    @RequestMapping(value = "set_user_character", method = RequestMethod.POST)
    public JsonData setUserCharacter(int user_id,@RequestParam(value = "characters") List<String> characters){
        userService.setUserCharacters(user_id,characters);
        StringBuilder sb = new StringBuilder();
        characters.forEach(sb::append);
        logger.info("setUserCharacter params: user_id:"+user_id
                + ",characters: " + sb.toString()
                +",return: success");
        return new JsonData(200,"success");
    }

    @ApiOperation(value = "获取用户粉丝")
    @RequestMapping(value = "get_fans", method = RequestMethod.GET)
    public JsonData getFans(int user_id, int page_num,int page_size){
        List<UserDO> userDaoEntities = userService.findFans(user_id,page_num,page_size);
        List<BriefUserVO> fans = BriefUserVO.convertBriefUser(userDaoEntities);
        logger.info("getFans params: user_id:"+user_id
                + ",page_num: " + page_num
                + ",page_size: " + page_size
                +",return: " + fans);
        return new JsonData(200,fans);
    }

    @ApiOperation(value = "获取关注用户")
    @RequestMapping(value = "get_focus_users", method = RequestMethod.GET)
    public JsonData getFocusUsers(int user_id, int page_num,int page_size){
        List<UserDO> userDaoEntities = userService.findFocusUsers(user_id,page_num,page_size);
        List<BriefUserVO> users = BriefUserVO.convertBriefUser(userDaoEntities);
        logger.info("getFocusUsers params: user_id:"+user_id
                + ",page_num: " + page_num
                + ",page_size: " + page_size
                +",return: " + users);
        return new JsonData(200,users);
    }

    @ApiOperation(value = "搜索用户")
    @RequestMapping(value = "search_users", method = RequestMethod.GET)
    public JsonData searchUsers(String user_name, int page_num,int page_size){
        List<UserDO> userDaoEntities = userService.searchUsers(user_name,page_num,page_size);
        List<BriefUserVO> users = BriefUserVO.convertBriefUser(userDaoEntities);
        logger.info("searchUsers params: user_name:"+user_name
                + ",page_num: " + page_num
                + ",page_size: " + page_size
                +",return: " + users);
        return new JsonData(200,users);
    }

    @ApiOperation("绑定地址")
    @RequestMapping(value = "bind_address", method = RequestMethod.GET)
    public JsonData bingAddress(int user_id, String address){
        userService.bindAddress(user_id,address);
        logger.info("bingAddress params: user_id:"+user_id
                + ",address: " + address
                +",return: success");
        return new JsonData(200,"success");
    }

    @ApiOperation("用户认证")
    @RequestMapping(value = "authenticate_user", method = RequestMethod.POST)
    public JsonData authenticateUser(String academy, String major, String studentNum,
                                              String name, String cardUrl, Integer id){
        userService.authenticateUser(academy, major, studentNum, name, cardUrl, id);
        logger.info("authenticateUser params: academy:"+academy
                + ",major: " + major
                + ",studentNum: " + studentNum
                + ",name: " + name
                + ",cardUrl: " + cardUrl
                + ",id: " + id
                +",return: success");
        return new JsonData(200,"success");
    }

    @ApiOperation("获取用户认证状态")
    @RequestMapping(value = "get_authentication_state", method = RequestMethod.GET)
    public JsonData getAuthenticationState(Integer id){
        boolean authenticationState = userService.getAuthenticationState(id);
        logger.info("authenticateUser params: id:"+id
                +",return: " + authenticationState);
        return new JsonData(200,authenticationState);
    }

    @ApiOperation("查询用户是否报名")
    @RequestMapping(value = "is_applied", method = RequestMethod.GET)
    public JsonData isApplied(Integer user_id,Integer activity_id){
        boolean isApplied = userService.isApplied(user_id, activity_id);
        logger.info("authenticateUser params: user_id:" + user_id
                + ",activity_id: " + activity_id
                +",return: " + isApplied);
        return new JsonData(200,isApplied);
    }

}
