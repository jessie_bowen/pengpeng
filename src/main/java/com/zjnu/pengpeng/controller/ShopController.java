package com.zjnu.pengpeng.controller;

import com.zjnu.pengpeng.controller.entity.CategoryVO;
import com.zjnu.pengpeng.controller.entity.JsonData;
import com.zjnu.pengpeng.dao.entity.BriefItemDO;
import com.zjnu.pengpeng.dao.entity.CategoryDO;
import com.zjnu.pengpeng.dao.entity.ItemDO;
import com.zjnu.pengpeng.service.ShopService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author ZhaoBW
 * @version 1.0
 * @date 2020/11/6 14:52
 */
@Api(tags = {"商城API"})
@RestController
@RequestMapping("api/shop")
public class ShopController {

    @Autowired
    ShopService shopService;

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @ApiOperation("查询商品分类")
    @RequestMapping(value = "find_category_by_shop_id",method = RequestMethod.GET)
    public JsonData findCategoryByShopId(int shopId){
        List<CategoryDO> categoryDOList = shopService.findCategoryByShopId(shopId);
        List<CategoryVO> categoryVOList = CategoryVO.convertList(categoryDOList);
        logger.info("findCategoryByShopId: " +
                "param: shopId:" + shopId +
                ", return: " + categoryVOList);
        return new JsonData(200,categoryVOList);
    }

    @ApiOperation("根据商品分类查询商品")
    @RequestMapping(value = "find_item_by_category",method = RequestMethod.GET)
    public JsonData findItemByCategory(int categoryId,int pageNum,int pageSize){
        List<BriefItemDO> briefItemDOList = shopService.findItemByCategory(categoryId,pageNum,pageSize);
        logger.info("findItemByCategory: " +
                "param: categoryId:" + categoryId +
                ", pageNum: " + pageNum +
                ", pageSize: " + pageSize +
                ", return: " + briefItemDOList);
        return new JsonData(200,briefItemDOList);
    }

    @ApiOperation("查询商品详情")
    @RequestMapping(value = "find_item_by_id",method = RequestMethod.GET)
    public JsonData findItemById(int id){
        ItemDO itemDO = shopService.findItemById(id);
        logger.info("findItemById: " +
                "param: id:" + id +
                ", return: " + itemDO);
        return new JsonData(200,itemDO);
    }

    @ApiOperation("查询店铺所有商品")
    @RequestMapping(value = "find_item_by_shop",method = RequestMethod.GET)
    public JsonData findItemByShop(int shopId, int pageNum, int pageSize){
        List<ItemDO> itemByShop = shopService.findItemByShop(shopId, pageNum, pageSize);
        logger.info("findItemByShop: " +
                "param: shopId:" + shopId +
                ", pageNum: " + pageNum +
                ", pageSize: " + pageSize +
                ", return: " + itemByShop);
        return new JsonData(200,itemByShop);
    }

}
