package com.zjnu.pengpeng.controller;

import com.zjnu.pengpeng.controller.entity.BannerVO;
import com.zjnu.pengpeng.controller.entity.JsonData;
import com.zjnu.pengpeng.dao.entity.ActivityDO;
import com.zjnu.pengpeng.dao.entity.BannerDO;
import com.zjnu.pengpeng.service.BannerService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author ZhaoBW
 * @version 1.0
 * @date 2020/11/6 14:52
 */
@Api(tags = {"轮播图操作"})
@RestController
@RequestMapping("api/banner")
public class BannerController {

    @Autowired
    BannerService bannerService;

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @ApiOperation("查找所有轮播图")
    @RequestMapping(value = "get_all_banner",method = RequestMethod.GET)
    public JsonData getAllBanner(){
        List<BannerDO> allBanner = bannerService.findAllBanner();
        List<BannerVO> result = BannerVO.convertBanner(allBanner);
        logger.info("getAllBanner: ,return: " + result);
        return new JsonData(200,result);
    }



}
