package com.zjnu.pengpeng.controller;

import com.zjnu.pengpeng.config.BosFileConfig;
import com.zjnu.pengpeng.controller.entity.JsonData;
import com.zjnu.pengpeng.interceptor.CustomException;
import com.zjnu.pengpeng.util.FileUtils;
import com.zjnu.pengpeng.util.WechatUtils;
import com.zjnu.pengpeng.util.baiduutil.BaiduUtil;
import com.zjnu.pengpeng.util.baiduutil.Base64Util;
import com.zjnu.pengpeng.util.baiduutil.HttpUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import java.io.IOException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author zbw
 */
@Api(tags = {"文件操作"})
@RequestMapping("api/file")
@RestController
@PropertySource("classpath:/application.yml")
public class FileController {

    @Autowired
    FileUtils fileUtils;

    @Value("${file_path}")
    String filePath;

    @Autowired
    BosFileConfig fileConfig;


    private final Logger logger = LoggerFactory.getLogger(getClass());

    @ApiOperation(value = "上传文件")
    @RequestMapping(value = "upload_file", method = RequestMethod.POST)
    public JsonData uploadFile(@RequestParam("file") MultipartFile file) throws IOException {
        Boolean checkImg = WechatUtils.checkImg(file.getInputStream(), file.getContentType());
        if(checkImg){
            throw new CustomException("图片内容违规，请重新上传");
        }
        String path = filePath + fileConfig.uploadFile(file);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String curDate = sdf.format(new Date());
        logger.info("uploadFile: "+curDate+",url:"+path);
        return new JsonData(200,path);
    }

    @ApiOperation(value = "学生卡识别")
    @RequestMapping(value = "identify_card",method = RequestMethod.POST)
    public String identifyCard(@RequestParam("file") MultipartFile file) throws Exception {
        //高精度版
        String url = "https://aip.baidubce.com/rest/2.0/ocr/v1/accurate_basic";
        //标准版
//        String url = "https://aip.baidubce.com/rest/2.0/ocr/v1/general_basic";

        byte[] imgData = file.getBytes();
        String imgStr = Base64Util.encode(imgData);
        String imgParam = URLEncoder.encode(imgStr, "UTF-8");

        String param = "image=" + imgParam;
        String accessToken = BaiduUtil.getAuth();
        return HttpUtil.post(url, accessToken, param);
    }


}
