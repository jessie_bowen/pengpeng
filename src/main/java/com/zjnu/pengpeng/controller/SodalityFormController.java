package com.zjnu.pengpeng.controller;


import com.zjnu.pengpeng.controller.entity.JsonData;
import com.zjnu.pengpeng.controller.entity.SodalityFormVO;
import com.zjnu.pengpeng.dao.entity.ActivityDO;
import com.zjnu.pengpeng.service.SodalityFormService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


/**
 * @author ZhaoBW
 * @version 1.0
 * @date 2020/11/21 10:08
 */
@Api(tags = {"联谊活动"})
@RestController
@RequestMapping("api/sodality_form")
public class SodalityFormController {

    @Autowired
    SodalityFormService sodalityFormService;

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @ApiOperation("提交申请表")
    @RequestMapping(value = "post_sodality_form",method = RequestMethod.POST)
    public JsonData postSodalityForm(SodalityFormVO formVO){
        int id = sodalityFormService.insertForm(formVO);
        logger.info("postSodalityForm: param: " + formVO +
                ",return: " + id);
        return new JsonData(200,id);
    }

    @ApiOperation("查询申请表")
    @RequestMapping(value = "get_sodality_form",method = RequestMethod.GET)
    public JsonData getSodalityForm(int page_num,int page_size){
        List<SodalityFormVO> result = sodalityFormService.findSodalityForm(page_num,page_size);
        logger.info("getSodalityForm: param: page_num" + page_num +
                ",page_size: " + page_size +
                ",return: " + result);
        return new JsonData(200, result);
    }

    @ApiOperation("查询本次活动")
    @RequestMapping(value = "get_activity",method = RequestMethod.GET)
    public JsonData getActivity(){
        ActivityDO activityDO = sodalityFormService.getActivity();
        logger.info("getActivity: ,return: " + activityDO);
        return new JsonData(200,activityDO);
    }

}
