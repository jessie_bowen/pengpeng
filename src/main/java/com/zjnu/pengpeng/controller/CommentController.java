package com.zjnu.pengpeng.controller;

import com.zjnu.pengpeng.controller.entity.CommentVO;
import com.zjnu.pengpeng.controller.entity.JsonData;
import com.zjnu.pengpeng.dao.entity.CommentDO;
import com.zjnu.pengpeng.service.CommentService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;

/**
 * @author zbw
 */
@Api(tags = {"评论操作"})
@RequestMapping("api/comment")
@RestController
public class CommentController {

    @Autowired
    CommentService commentService;

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @ApiOperation(value = "提交帖子评论")
    @RequestMapping(value = "comment_post", method = RequestMethod.POST)
    public JsonData commentPost(CommentVO controllerEntity) throws IOException {
        commentService.commentPost(controllerEntity);
        logger.info("commentPost: params:" + controllerEntity
                +",return: success");
        return new JsonData(200,"success");
    }

    @ApiOperation(value = "获取帖子评论")
    @RequestMapping(value = "get_comment",method = RequestMethod.GET)
    public JsonData getComments(int post_id, int page_num, int page_size){
        List<CommentDO> daoEntityList =
                commentService.findCommentByPostId(post_id,page_num,page_size);
        List<CommentVO> controllerEntityList =
                CommentVO.convertComment(daoEntityList);
        logger.info("getComments: params:"
                + ",post_id: " + post_id
                + ",page_num: " + page_num
                + ",page_size: " + page_size
                +",return: "+controllerEntityList);
        return new JsonData(200,controllerEntityList);
    }


}
